被放在桌子上的是一把短劍
整體造型很簡潔，但劍柄的部分卻帶有精緻的紋章
和那個商人拿著的並不一樣
但是

「⋯⋯看起來挺貴的」

我無意中露出了窮相，羅蕾露有些無語的看著我

「現在的你，應該出的起錢吧。雖說如此，能不能買到就不好說了。本來，這也不是一般會拿來賣的東西」

說了一番意味深長的話

「這是什麼意思⋯⋯？嗯⋯⋯？」

我這樣說著，觀察起那柄短劍，想要將其拿起來的時候，卻有種非常不快的感覺

「怎麼回事？」

我將短劍放回桌上，愛麗澤似乎也很意，說著「失禮了」的話，伸出手去
但羅蕾露卻阻止了她

「⋯⋯你最好別這樣」
「哎？⋯⋯為什麼呢？」

愛麗澤不可思議地歪著頭
相對的，羅蕾露回答

「我和那傢伙對這種東西有耐性。但是，一般人直接接觸的話，可能會產生不好的影響。至少，那個商人都會用這種東西包起來」

說完後，她從懷裡取出折好的布

「⋯⋯這是⋯⋯原來如此。帶著聖氣的布，也就是說，這把短劍⋯⋯」
「沒錯，這是咒物，本來是被禁止帶入馬路特的」

咒物

也就是詛咒道具、惡魔的道具、暗具，有著各種各樣稱呼的，一種特殊道具
舉個簡單例子的話，就是我臉上的這個
雖然據說這實際上是神具，和咒物應該有所不同
但最初我確實以為這是什麼詛咒道具
因為摘不下來嘛⋯⋯嘛，以結果來說，我算是很幸運的
至少找到了不用把臉露出來的藉口
因為面具摘不下來

「咒物⋯⋯！？這就是麼⋯⋯我還是第一次看到，但光看著也不很明白⋯⋯」

愛麗澤說著睜大了眼睛
這有些令人意外呢
羅蕾露也和我一樣這樣覺得麼，問到

「嗯，『學院』裡不是應該也有咒物的麼？」

「學院」本質上是教育機構，但也有作為研究機構的一面
而且，其研究對象不僅僅是普通的學問，與魔術相關的很多方面都有涉及

其中，當然也應該包含咒物，專門研究這些的學者應該也有吧
咒物雖然大體上算是魔法道具的一類，但往往會無視通常的魔術法則
解析其效果與構造，會在各種方面帶來幫助
這是羅蕾露說過的話

愛麗澤回答

「雖說是『學院』，但咒物本身就是貴重的東西，所以沒法輕易見到⋯⋯聽說『塔』那邊有好幾個，但也都受到嚴密保管，不會輕易向學生展示的」

羅蕾露點了點頭

「是這樣麼⋯⋯嗯，我知道了。這麼說來，這也算是個難得的機會，要不要摸摸咒物？」

出乎意料地這樣說到

「羅蕾露⋯⋯剛剛不是才說過會有不好的影響嗎？沒關係麼」

我這麼一問，她回答

「哎呀，我的意思不是在這裡。畢竟是在街上，發生什麼意外情況就不好了呢，但愛麗澤之後應該還會在馬路特待一段時間吧？如果有這想法，可以帶上其它幾個人，另約時間地點，如何呢？」

相當危險的提案呢，但羅蕾露的性格就是如此
雖說也沒有什麼了不起的危害啦

嘛，咒物也分很多種類
有像我的面具一樣令人束手無策的，但這只是極少數，大部分咒物都能夠被教會的聖人、聖女淨化
這把短劍就是如此吧⋯⋯大概
就算不是這樣，隔著含有聖氣的布觸摸，應該也沒什麼問題

聽到羅蕾露的提議，愛麗澤沉思了一會兒，身為學院的學生，面前有學習的機會，實在不想放棄吧
她露出下定決心的表情

「那麼，可以拜託您嗎？因為需要和別人商量一下，所以沒法現在決定。一旦商量好了了，就馬上和您聯繫」

這樣說到
羅蕾露點點頭，和她交換了聯繫方式
學院的學生們包下了馬路特的大旅館呢
應該連「塔」的房間也一起訂了吧，真有禮貌啊⋯⋯嘛，邊境都市的旅店，沒什麼了不起的
之後，愛麗澤說約好了集合時間，先行回旅店了
臨走還依依不捨地和莉娜擁抱在一起，說著「什麼時候一起吃個飯吧」之類的話

我看著她們的模樣，向羅蕾露問到

「那個商人就是用這樣的短劍扭曲了長袍的術式麼？」

雖然拿在手上還是會覺得很討厭，但已經習慣了
羅蕾露點點頭

「啊，大概是具有擾亂附近物品上魔力的效果吧。本來，為了不發生這種事情，魔道具中都會設置防護措施，想要越過防護產生影響⋯⋯以現在的技術很難辦到」
「也不是不可能辦到」
「沒錯，因為有各種各樣的方法啊。話雖如此，『學院』的長袍也應該針對既存的破壊手段採取了各種措施。但這些在咒物的詛咒面前看來還是有所不足。如果能分析那東西的結構，一定能帶來莫大的財富吧，但看上去應該很難」

羅蕾露擁有魔眼
如果是普通的魔道具，應該可以一眼看穿構造，不過咒物似乎沒那麼簡單

「順便問一句，我和羅蕾露拿著這個也沒關係的理由是？」
「我們有祠堂分神的加護。和隔著帶聖氣的布接觸差不多，就是這麼回事。莉娜從你那裡借來聖氣的話應該也沒問題⋯⋯不過關於這件事，剛才不方便說明」

說的也是
在愛麗澤面前說這些話，難免被問到，是在什麼時候得到聖氣的
嘛，之後再告訴莉娜就好了吧