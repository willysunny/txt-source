Elf（精靈種）的始祖，是存在於與這個物質界不同時空的精靈界移居而來的者們。

也就是可以說是生存在物質界與精靈界之間的夾縫的種族。

精靈根據生活棲息地被稱為Glass Elf（草原精靈），Sea Elf（海精靈），Mountain Elf（山脈精靈）等，最為人所知的就是與森林的植物友好生存的Wood Elf（樹精靈）。

現在包圍我們的正是這些Wood Elf（樹精靈）們。

看見身影的Wood Elf（樹精靈）有四人。不過巧妙的用森林色彩潛藏著氣息，不見身影地監視著我們的有三人。

想到那些隱藏者的存在，也就是說對我們的警戒沒有解緩吧。

過去跟貝倫村的人們交涉過的Wood Elf（樹精靈）們，並未有如此程度的對人類是抱有厭惡感，或是敵對視。

這麼看來，應該認為他們達到過剩的警戒心，表示著恩特之森發生的事態已經迫在眉睫，並奪去了他們平常的餘裕平靜了麼。

Wood Elf（樹精靈）的其中一人向我們正面前來，十步遠的距離處停下了腳步。

是一名深綠色的頭巾纏著淡淡的金髮，令人聯想到猛禽類般銳利眼神的青年。

看來是森林枝葉伸張的巨大葉片的葉脈紡織而成的綠色衣服包裹著身體，細長的手指握著製作精良的短弓。

雖然箭矢沒有在弦上，不過右手已經伸到腰間的箭袋上，一旦我們有什麼可疑的行為的話，這青年的雙手毫無疑問會像閃光般的做出射箭的行動。

眼前出現的青年，僅僅是感覺聯想便給人如此不同凡響的可怕感。就這樣出現面前讓我們看到身影也因為受到周圍的Wood Elf（樹精靈）們的信賴而有的行動吧。

我輕易地可以想像得到，通常面對危險立於先頭，毫無疑問地站出來保護同伴們的作風。

青年用尖銳且包含著擔心的嗓音對夾在我們與他們之間慌張與困擾的瑪露說話。

「瑪露，離開他們過來這裡！」

「基歐，可是，這些人類桑們救了瑪露得嘶喲」

「我知道。只要他們不動手我們也不會加以危害。所以，過來這邊，快點」

瑪露被稱為基歐的Wood Elf（樹精靈）催促之下，回頭向我們大大的低下頭，一邊多次向我們回頭一邊飛回Wood Elf（樹精靈）們那裡去。

對經過基歐旁邊的瑪露，等候在基歐后面的Wood Elf（樹精靈）少女浮現明朗的笑容說道。

看著是打從心裡擔心著的模樣，應該平日裡就跟瑪露的關係很是親密吧。

「瑪露，不是說了那麼多次跟你說了不能自己一人出去外面的嗎。現在的森林可是非常危險的喲」

「對不起，菲歐。可是，無論怎樣都很在意外頭的情況。森林的大家現在，到底怎麼了，我想知道的說」

「瑪露的心情我也是切身體會著的啊。但是，再自己一個人跑去外面的話不行哦。瑪露若是有個什麼的話，我可是會非常擔心呢」

感覺跟基歐樣貌有些相似的少女，大眼睛裡含著眼淚訴說著，瑪露則對令很重要的朋友為自己擔心一事，不斷地道歉說著對不起。

看到這樣的兩人，打從心裡覺得救了瑪露真的是太好了。

緊接著，基歐似是要將稍有鬆緩的氣氛再次緊繃一樣，再一次陡著眼梢向我我們轉過身來。

基歐那與樹木如出一轍的綠瞳目不轉睛地盯著我的臉。想要找出我們的真意似的，他的視線徑直地射向我的眼睛。

「人類和拉米亞喲。首先對救了吾等之友瑪露的事向你們致敬。謝謝。

但是，這森林是包含吾等Wood Elf（樹精靈）在森林中生活者的領域。

『只要人類們不侵犯森林我等亦不侵犯人類的領域』

過去跟治理這附近的人類們是這樣交換約定的。為何破壞其，踏足於森林？」

首先致敬瑪露的事那份禮貌，讓我對基歐有了好感。只要不會突然射箭過來，好好的說明的話就能回避紛爭吧。

我把理應在恩特之森棲息的野獸在村子附近出沒，推測森林發生了異變所以前來調查一事，跟已告知瑪露的一樣說明著。

實際上我的推測也正中了靶心，恩特之森中確實有魔兵出沒的異常事態到來。

聽了我的說明，基歐幾乎要說出“果然”地深深皺眉成川字。

周圍潛伏著的Wood Elf（樹精靈）們也開始動搖，如搞亂了精神的水面般，漣漪不絕，細細交頭接耳起來。

菲歐那天真爛漫的容貌上明顯的出現陰影，一副對森林的異變甚至波及影響到森林之外的事實感到悲傷的表情。

「如果是這樣的理由的話，不會追究你們踏入森林一事。然後，如果再次和魔兵交戰的話，什麼都不告訴就趕你們回去也不行。

現在，這片恩特之森正跟魔界者們發生著戰爭。在你們村子附近見到的，應該就是被這紛爭卷入而被從森林驅逐出去者們吧」

「魔界的大軍！？」

從基歐口中得知的事實令克里斯汀娜頓時張口結舌。

基本上所有的人類在一世生涯終臨為止，魔界都完全與之無緣。聽到同那樣的異界存在接觸，克里斯汀娜似乎無法抑制住那份驚訝。

對我來說在自己預料的兩個可能性中，猜中了最麻煩的那個令我在心中咬碎了苦虫，愁眉苦臉起來。

曾經，從善神居住的天界和惡神居住的魔界都可以自由往來人間界。

不過在我還生為龍的時候爆發了神與神之間的壯烈戰爭，其結果，人間界和天界與魔界之間，出現了即使是神魔也無法輕易通過的擴及複數的次元・時空的斷層。

這個世界間的斷層構築是跟吾之同胞的龍種有關，不過現在這些還是先放在一邊。

這個世界間的斷層很難說得上是完全隔絕，有如網狀細小的洞口打開著的狀態下，地位高且能力高的強者會被卡住在這洞口，要出現在人間界――物質界是很困難的。

假使可以出現，也無法可以發揮十全的力量或異能，就算是高位的神也只能做到向地上傳達微小的力量與語言這種程度。

反而力量比較弱的人或低級的存在的話——例如澤嚕托這樣的魔兵的話——就能在能力不下降的情況下降臨。

可是就算是澤嚕托這樣的下級魔兵，以百為單位的集團出現在地上的話也屬非尋常之事。

也就是說恒常接駁著魔界的『門』在恩特之森裡被打開了嗎？

如果維持著前世作為龍種的感覺的話，門打開之際就會發現空間的異常……不，事到如今懊悔也太遲了，晚了吶。

比起做不到的事該考慮今後能做到的事才對。必須盡早關閉聯繫著魔界的門。

長時間放置門不管的話，這邊的萬物將會被魔界之氣侵蝕，魔界化嚴重的話，高位的惡魔和魔族都會出現在地上。

這麼一來為地上帶來的破壞與死亡與恐怖將是無可計量的吧。

『在那樣之前，將來到這邊的魔界者們全部泯滅葬送！』

在我心中感覺燃燒著黑暗感情的火炎。

對凡是危害到我現在的家族與朋友者，我進行任一容赦的打算都不會存在！

即便是我的靈魂是龍一事被知道而被逐出故鄉，我仍舊會毫無躊躇地為了保護村子的大家而揮展力量。

我在心中考量著關於魔界者們的事之時，受到出現魔界大軍這異常事態的衝擊的克里斯汀娜以像要吃掉基歐般的氣勢重複質問著。

雖說很少會有關連的事，但在過去幾度出現在地上的魔界者們的傳聞即使是人類也是知曉的，不論哪一次都是為塵世帶來慘不忍睹、任何一個人想都不願去想的被害。

若是有可能發生那傳說的再現的話，不論是誰都不能対之泰然處之，平靜自如吶。

「那麼，那些魔界者們是從幾時開始出現的？這事態跟王國通告過了嗎？您們有什麼樣的對應方法？」

「克、克里斯汀娜桑，請不要這麼慌張，妳看，基歐桑也感到為難了( >Д< ) 」

還閉著眼的賽莉娜慌慌張張地開口阻止著毫無克里斯汀娜風格的姿態。

賽莉娜看上去跟克里斯汀娜相比起來比較冷靜，是因為有著比自己更慌張的克里斯汀娜和我維持著冷靜的原因吧。

再加上對賽莉娜來說，就算被告知了魔界大軍也絲毫沒有一點現實感也說不定。

「賽莉娜，可是這並不是尋常的話……不，說的也是。很抱歉。我有點驚慌失措了」

「不，你們的慌張也能理解。但是，這是吾等的問題。侵犯森林者們，生存在森林裡的吾等必定將其打倒。此並非理應與你們有關之事。你們就這樣向外歸去便足以。

待不久森林的異變平息後，在你們領土的森林之民也會變為從你們的眼中消失的。恩特之森的Wood Elf（樹精靈）基歐，以吾一族榮耀之名做出許諾」

向我這麼發誓的基歐臉上盡是堅定的決心與榮耀。

可是對手是魔界者們的話，也不能全部交任給Wood Elf（樹精靈）們處理。

雖不知道恩特之森的戰力到什麼程度，但就算只有少許戰力也非常需要的吧。

「基歐，雖並不是在懷疑您所說的一切，但唯獨這次不能那樣坦率地聽從您的話去做。

若是對生存於地上的所有生命而言都是敵人的魔界者們到來的話，我們做不到就這樣垂頭喪氣的回村子去。

至少，也想要用這雙眼親眼確認魔界者們的戰力與動向。

當然，我們能幫助到的事也會盡力而為。我是這麼想的，克里斯汀娜桑和賽莉娜覺得怎樣？」

「沒有做出否定的回答的打算。到底變成怎樣的狀況了，我也是想要親眼確認吶」

「如果多蘭桑和克里斯汀娜桑不回去的話，我也不回去。而且剛才看見叫澤嚕托的魔兵時，感覺非常討厭。（;≥皿≤）

那是不可以在這世界的存在得嘶。只有基歐桑他們也能轟走的話那就再好不過了，但我想我們也能略盡綿力的。| ू•ૅω•́)

不，倒不如說反而是我們這邊比較需要基歐桑他們的援手沒錯吧？」

克里斯汀娜以常人無法相比的靈魂與本能的領域理解感覺到魔界的人們的危險性，甚至不惜至今為止的人生所培育的倫理觀與道德觀也要得到Wood Elf（樹精靈）們的幫助的樣子。

然後是賽莉娜，看來她比我想像中還要理解事態的危險性。是流走在身體內的魔蛇詛咒察覺到近處的同樣詛咒的而在吵吵嚷嚷著嗎。

對於我們三人都拒絕離開森林，基歐大大的嘆了口氣，無力的搖搖頭。

「很感謝你們提出的鼎力相助。不過沾污森林者必要由吾等住在森林裡的人打到不可。這是森林的規定。這是現在甚至今後也不會改變，也不能改變的規定！」

「可是我們就這樣在說話期間，森林和住在森林裡的人們都被魔界的人們威脅著對吧？那麼我希望能暫時放下規定跟我們一起作戰。

規定是為了生命而定，為了守護規定而犧牲生命的話不就本末倒置了嗎？

與魔界的人們的戰鬥告一段落後，或是得到了足夠的情報的話便按照您所說的一樣離開森林。不知能否考慮下嗎？」

「……對你的提議，很是感謝。可是……」

Fumu，基歐本身也很清楚事情已經迫在眉睫了，可是，即便如此似乎還是以森林生存者的榮耀和使命感為上，無法坦率地接受我們的提議的樣子吶。

基歐旁邊的菲歐和隱身在周圍的Wood Elf（樹精靈）們都猛咽唾液的等待基歐的判斷。

對於我們形如流水地打倒澤嚕托的傑出能力，對他們來說是從喉嚨伸出手一樣的渴望得到吧，只是要接受從森林外來者，在心情上還是會很糾結吧。

雖然知道這點，但就這樣僵持在問答中，只是浪費時間的惡作劇，反而只會有益於魔界者們。

我們沒有回頭的打算，所以無論怎麼樣都必須說服基歐好得到魔界的人們的情報，達成協議不可。

『那麼要怎樣說服呢？』

不只是我，克里斯汀娜和賽莉娜都同樣在考慮之時，收起羽翅站在菲歐肩上的瑪露怯生生的開口了。

「基歐，多蘭桑，你們兩人所說的瑪露覺得兩邊都非常的沒錯。可是，太陽公公已經開始下山了。再這樣說下去的話，天會到晚上的哦？」

正如瑪露所言太陽開始傾斜，持續下沉向西邊著。到達恩特之森的時候已經是中午時分。

那之後就一直往森林深處不斷前進的現在，天空的天藍色已經漸漸的跟薄紫色交融了。

魔界的人們有著喜好夜暗和夜間氛圍的夜行性傾向，在夜幕會更活躍。在森林當中迎接黑夜，毫無疑問是極為危險的行為。

瑪露的指意對基歐就不同意思了，端正的容貌變得嚴肅，看著我們的眼睛浮現著迷惘。

「哥，再這樣談論下去事情也沒有著落哇。

就算讓這些人就這樣出去森林外面，在途中就入夜了也很危險，這裡先帶他們到村子去，然後在村裡再告訴他們我們所知的事情吧。

那樣做的話這些人也能接受到某種程度，或許會改變初衷也說不定。而且我們也在入夜前沒回到村子的話……」

「菲歐……的確，跟妳和瑪露說的一樣吶。沒辦法。你們，只有今夜逗留在偶們村裡吧。現在這時期留在森林迎接夜晚太危險了。

本來的話是不能隨便讓外來者進村的，不過考慮到現在的狀況的話也沒辦法了」



「知道了。感謝厚情相待。劍先交給您們保管比較好嗎？」

對於我的問題，基歐細微地搖了搖頭。

「不，拿起來吧。雖不好意思不過自己的安全還請自己保護吧。偶們也沒多少的有餘吶」

是因為考慮到回村的路上會被魔兵襲擊的可能性的話，從難得擁有戰鬥能力的我們手中取走武器的話並不好，這麼認為吧。

「我們三人正這麼打算，自己照顧自己。賽莉娜，張開眼睛也沒問題了喲」

拾起放在腳邊的長劍，將劍鞘掛在腰間的Belt（腰帶）上。

克里斯汀娜和賽莉娜兩人也迅速結束了準備。

隱身在周圍的Wood Elf（樹精靈）們也展現身影，基歐他們包圍在我們周圍一路向他們的村子方向在森林中前進。

原本就因樹枝覆蓋在頭上而陰暗的森林，在太陽的傾斜開始之下急速拓展黑暗的領土，加深了步伐之間樹木落在地上的陰影。

因拉米亞的賽莉娜擁有熱能感應的能力，所以在黑暗中也沒有任何不便，我和克里斯汀娜則不可以那樣。

如果不雙瞳通過魔力，變成魔眼來賦予魔法視力的話，要在夜幕降臨的森林裡確保視線的清晰是很困難的。

變成連遙遠距離的對象一根毛，一個毛孔，不分明暗也能捕捉到其的魔眼，克里斯汀娜的赤瞳如血色覆染著的滿月般發出妖艷的光芒。

好像隨著月光改變姿態的銀髮，美神會妒忌也不出為奇的美貌，柔眸泛著赤顏，一切都散發著像魔性般過人的姿態。

就算是擁有能魅惑其他種族之術，同時自己也是極品美少女的賽莉娜，有時看著克里斯汀娜也會冷靜不下來，便是因為克里斯汀娜身上纏繞著的那份連同性都會被無一漏網之魚被之魅惑的妖艷之美。

Wood Elf（樹精靈）們以我們為中心形成圓形陣，不過冷不防打亂這圓形的菲歐帶著瑪露向我們靠近走來。

由於Wood Elf（樹精靈）是長壽的種族，因此很難猜出實際年齡，僅看外貌的話和我差不多年齡的菲歐，她翡翠色的柔瞳中流淌著好奇之意。

姑且不論實際年齡只談精神年齡，或許這位Wood Elf（樹精靈）少女的精神年齡如表一致也說不定。

「抱歉呢，明明特地到森林來了卻被卷入這樣的事中」

「非常抱歉得蘇…」

「請不要放在心上」

感覺在菲歐的右肩上無精打采著的瑪露的樣子是招人微笑之物的同時，我如此回答著。

「不是你們的緣故哦。話聽來你們才是遭受著大事件的樣子吶。以魔界大軍作為對手絕對不是件有趣的戦鬥對吧。

有可能得到其他的Wood Elf（樹精靈）的幫助或森林者們的協助嗎？在演化成到人類軍隊出手這樣的事態之前，不擊退魔界者們的話可不行。

不管怎麼說，讓人類踏入森林並不感覺會帶什麼太好的結果吶」

我自己對掌管王國政治者是怎樣的人類，完全沒有所知，但對作為集團叫人類的種族，還是不要太過期待會得到善意的對待比較好，作為前世學來的痛苦經驗分享。

真是現在回頭看也不想學會的事情吶。

「當然我們一定會將魔界的那班傢伙們什麼的全部都幹掉的哇！不僅僅是我們Wood Elf（樹精靈），在森林生存著的其他種族也會鼎力相助的哦。

而且其他的同胞們也應該會在近日也會前來幫忙。那樣的話魔界來的那些傢伙之類的就等著被我們打得落花流水吧～」

「落花流水得嘶！」

模仿「呼呼o(ˉⅴˉˇ)o」地自傲的說著的菲歐，瑪露也「誒哼o(≧v≦ˇ)o」這樣的挺起小小的胸膛。真是令見者心祥和的卡哇伊二人。

「這樣啊，打得落花流水麼。哈哈，兩人都那麼精神是好事。可以的話我也想幫助你們呢，如果基歐准許就好了」

「因為哥有些頑固不得知，不過我想要不要借助你們的力量是由村長們決定哇。

雖然我想現在出去外頭的人們也要回來了，就算不借助你們的力量也會有辦法解決的呢」

Fumu，Elf（精靈）雖基本上不會離開自己熟悉的地方，不過也會出現少數憧憬外面世界而離開部落的年輕人。

這是基歐和菲歐有辦法可以聯絡到以前出去外面世界的人們，使用這點呼喚他們回村一事吧。

在外面世界漂泊經過風雨者的話，多少會有某種程度的戰術在身所以可靠吧。

周圍的Wood Elf（樹精靈）們完全沒有在意我們的交談的樣子，基歐也沒有責備妹妹的意思，一直提防著周圍的氣息與變化。

在什麼時候從哪會出現魔兵們的襲擊並不知曉以上，基歐他們的行動是正確的。

是因為同Wood Elf（樹精靈）們並未挑起事端的原因吧，多少有些緩過氣的賽莉娜輕聲地來到我右側邊，向菲歐她們微笑打招呼。

菲歐和瑪露對拉米亞的賽莉娜沒有戒備的舉止，可能在這森林裡有拉米亞，或者說蛇的亞人相識的人在麼。

「菲歐你們的村子是怎樣的地方呢？像瑪露醬的妖精桑也有很多位住在那的嗎？」

「我們的村子是在恩特之森最西邊的一個村落喲。和像瑪露一樣的花之妖精和Dryad（樹精）們一樣的樹木精靈一起生活哇。

雖然附近還有其他的如狼人或Arachne（阿剌克涅）們的村落在，不過現在正因為同魔界者們的戦鬥著而同心協力著呢。那些傢伙現在瞄準的是我們Wood Elf（樹精靈）的說」

「其他的Wood Elf（樹精靈）們都沒事吧？」

「誒—（沒事）。我們的村落跟最靠近的其他村落也有好一段距離，而且也沒有收到其他村落被襲擊的聯絡哇。

剛才也說了有收到附近村落聚集而來幫忙的聯絡。所以沒問題哦。你們才是，請在村子裡休息一晚到天亮後就乖乖的回去外面呢。

雖然以前來跟我們交涉的人類們似乎都很懂禮儀的樣子，不過對人類沒有什麼好感的人在村子也是有的哦。因為賽莉娜是拉米亞所以不是很清楚就是了呢」

狀況應該相當的迫切吧，但總感覺菲歐不但不認生而且還很是喜歡聊天的樣子，遇上對人很好的賽莉娜馬上打成一片，氣氛融洽地話題大開了。

偶爾話題的矛頭會轉去克里斯汀娜那裡，連絕對不能說是能言善道的克里斯汀娜都會在菲歐巧妙的催促下斷斷續續的回答著。

看著克里斯汀娜表情逐漸舒緩露出靦腆的笑容，總感覺是跨越了種族隔閡成為朋友一樣的三人。

菲歐她們的話像花開一樣蔓延向周圍的女性的Wood Elf（樹精靈）們去，最終她們也加入了進去。綳緊神經警惕著周圍的變得只有基歐和我，以及其他的男性Wood Elf（樹精靈）。

但就連男性Wood Elf（樹精靈）們偶爾也會對菲歐她們明朗的笑顏而嘴角微微上揚，真的在集中精神進行戒備麼？我感到有些奇怪吶。

我向走在前面的基歐背后出聲。這個青年的話應該是那種至少會責備一句的那種類型才是啊……？

「不阻止她們的交談這樣好嗎？太大聲會被魔兵們發現也說不定哦」

「就是因為會遇上魔兵們的襲擊，菲歐才故意保持著開朗的。是為了鼓勵每每被恐怖和不安籠罩著的大家吶。

就算知道是很勉強但也因菲歐的笑容得救了，所以偶做不到說出讓那傢伙停下的話呢。

不過現在的並不是強顏歡笑的樣子。大概是從以前開始便對森林外頭抱有興趣的原因就是了吶。

聲音不用在意也沒事。已經『請讓樹木的輕聲細語也不會泄漏』地拜託了樹木們了」

這麼說著的基歐看著妹妹的表情，那份對親人那種暖暖的愛情，確確實實地映入眼簾。

而且正如基歐所言周圍的樹木們，用樹幹反射著枝葉的摩擦聲和風的流動，確定了菲歐她們的聲音不會傳去外界。

「那麼，我也沒什麼可說的吶」

……………………………………………………………………

一邊聽著完全消去森林樹木的輕聲細語的說話聲，一邊繼續前進更深一層的綠色道路中，太陽快要完全下沉到地平線的另一端時，我那敏銳化了的感覺捕捉到異變。

儘管無關風的流動改變了或什麼的，可是樹木的騷動卻異樣的變大，在風中響起風之精靈在悲鳴，腳踏的地面傳來大地之精靈的吵嚷。

傾聽著森林異變的基歐，一副意想不到的樣子止住了腳步，發出驚愕的聲音。

「什麼！？那些傢伙的行動提前了嗎！」

不只基歐，菲歐和其它的Wood Elf（樹精靈）們也是，完全轉變了到此為止祥和的氣氛，混合了驚訝與恐怖的色彩染在臉上，令臉色蒼白。

「哥，不快點回去的話，村子的大家都！」

「啊啊啊、森林，風，大家都會被殺啊！？」

看見臉上盡是焦慮的菲歐和已經陷入接近半恐慌狀態的瑪露，克里斯汀娜和賽莉娜馬上察覺了異常。

雖然只有我和Wood Elf（樹精靈）們一樣聽到了森林的悲鳴，不過這兩人也注意到森林異變的氣氛，馬上進入了臨戰狀態。很快理解的兩人真是幫大忙了。

「你們，抱歉，但帶到你們到村子裡已經……」

遮斷了基歐回頭告知的話，我開始咏唱著魔法。

「風之法則啊    傾聽吾之聲    化為一時推助吾背之疾風吧    Wind accelerator（ウィンドアクセル  風之加持）」

干涉風的流動賜予對象風速的補助魔法。我將其以在場的全部人為對象發動了。

基歐似乎對突然包覆著自己身體的被賦予指向性的風感到吃驚的樣子。

我對基歐不容問答的說道。

「風的補助魔法，緊急時能派得上用場吧。村子被魔兵們襲擊了，不是嗎？」

「……是的。幫大忙了，但你們就從這裡開始」

「沒有回去的打算哦。都到這裡了沒理由回去的不是麼。於情於理」

「我同意多蘭說的。我們雖然能力微薄但也可以幫上忙的。已經想這麼說好幾次就是了吶」

「我也跟多蘭桑和克里斯汀娜桑想的一樣得嘶！」

「會有生命危險的。而且戰鬥就能理解的話村裡的大家都會對你們寄予期望。然後估計偶也會拜託吧。

不管從森林外來的你們是不是只是被森林裡的紛爭卷入」

「你很善良，基歐。但我們說過不介意這些的吶」

基歐聽到我們說的話露出了比喝了苦茶還要苦的苦臉，微低下了頭。

「抱歉。不甚感激」

對低下頭的基歐我這麼回答道。

「算不上什麼恩情。撒啊，走吧」

夜晚的黑暗下垂，在魔界的人們的瘴氣開始污染冷冷清澄的夜氣中，我們如風般疾馳著。（夜気：冷氣跟清淨氛圍，前面的清澄也是雙意）

以在前方等待的戰鬥為目標，為了驅逐不該在這世界的異者們欲浴血奮戰到底。

基歐他們生活場所是在森林裡幾許清泉和附近極為古齡的參天大樹為中心的村子，聽說有接近五百人的Wood Elf（樹精靈）和妖精們居住在此。

據說至今為止魔界者們的襲擊都是零散遭遇，但這一次的似乎是前所未有的襲擊規模。

我們以甩掉因月光而灑落於地之影的速度，專心致志、馬不停蹄地奔跑著。

時而跳入樹從之中，時而飛跳而過，風吹過濃密的頭髮，映入這樣勇往直前的我們眼簾的是，圍繞著荊棘與長春藤糾纏交織著的防壁的異形大軍。

守護著基歐他們Wood Elf（樹精靈）部落的防壁，現在正被魔界的人們施加猛烈的攻擊，欲將之擊破。

沒有一個篝火在燃燒，只有天上的月亮灑落的月光作為燈火，魔界者們甚至連喧鬧聲、哄叫聲或苦鳴聲都沒有，僅僅是往防壁聚集。

擁有虛假無情生命的魔兵們，其進軍宛如月亮盅惑我們從縫隙窺視皮影戲的惡夢。

沒有聲息，沒有死亡之前因恐懼而震抖的心靈，甚至失去的生命也沒有的魔兵們的行進，對於自然孕育者們而言，沒有比這更令人感到毛骨悚然之物了吧。

對無生命者們那扭曲的姿態，奔跑在我旁邊的克里斯汀娜和賽莉娜的喘息聲變得清晰可聞。

在魔兵們面前儼然聳立的城壁，這兒那兒綻放著赤、黃、紫、白、藍、綠等各色各樣的花兒。魔兵不小心碰觸到的話，突兀地會從花那伸出刺貫穿其身體，然後從傷口將構築魔兵的力量不漏絲毫地吸食到盡。

無論如何都不覺得是自然生成的防壁，是為了保護以Wood Elf（樹精靈）為首於森林生存者，樹木們以自己的意志所建成的牆壁，活生生的牆壁。

防壁的頂上更有有Wood Elf（樹精靈），獸人和虫人的身影。他們以聚集向防壁的魔兵為目標投石、射箭、咏唱魔法等，試盡最大限度的抵抗。

想打破防壁的魔兵們並不僅只有澤嚕托。

蠻力魔兵【扎爾滋（ザルツ）】。

比我高大數倍纏繞著鋼鐵般肌肉的盔甲，有著圓木般粗壯的手腕和沒有眼睛鼻子嘴巴平板無表情的臉。

然後，裝備著銳利爪子四腳獸的下半身具備騎槍的右腕和圓盾的左腕，頭盔裡湛放紅光的赤瞳，可以確認是魔兵【伽納弗（ガナフ）】。

仔細看去，在魔兵們的腳下，不計其數的和構建防壁同樣的荊棘從地面伸展，將組成部隊從魔兵腳部開始纏繞上直至絞殺，更有樁子般尖銳的樹根伸展刺穿其身體或大腿。

有著這麼多的妨礙，魔兵們似乎比想像中還要難靠近防壁。

可以說出乎我意料之外的Wood Elf（樹精靈）們對魔兵能征善戰。

不過抱有這樣的感想也只有到從後方巨大騎兵塵土飛揚奔來那的短短時間而已。

在向村子西南奔行而去的我們的視線之前，感覺到村子的北側有四名以魔兵們而言也是異常巨大力量的存在，而其中一名正以非比尋常的速度向村子的防壁迫近著。

腳一踢就能捲起大量沙土的巨大身影，跟伽納弗一樣下半身是四腳獸，手腕變形成巨大的騎槍和圓盾。

但是上半身和下半身的分界線有著一顆牙齒在嘎吱嘎吱作響沒有眼睛的獸顱在，上半身覆蓋著絲無縫隙的厚重盔甲。

血色的盔甲和同色的槍，盾也可以隨心所欲的變形成對應所有的身體需要。

那比我大上差不多三倍的巨型騎兵，別說其前行路上的魔兵同胞，從地面冒出來的荊棘和樹根也不放在眼裏的橫衝直撞，六只長爪踐踏而過，右腕的槍橫掃前進。

簡直就是沒有任何人能夠阻止的破壞突擊。

撕碎疾風般奔跑的巨大騎兵，發出了令人聯想到破鐘般低沉的吶喊。跟沒有自我的魔兵們不同，這個騎兵有自己的意識。

「滾開滾開！想被擊碎成塵埃的話就儘管站在本蓋歐魯多大爺面前來！！」

高調自稱為【蓋歐魯多（ゲオルード）】者，其周圍都是全身發出的紅色魔力形成的力場，急劇增幅著前面猶如一根角的一樣粗大的騎槍聚焦了的突擊破壞力。

踩爛了幾個澤嚕托和扎爾滋，那五個被踐踏得破爛不堪不留原型，終於蓋歐魯多的騎槍尖端突刺中保護村子的樹木防壁。

劇烈衝突的瞬間，纏繞著騎槍的紅色力場跟樹木防壁裡通過的精靈們的魔力激烈角逐，造成的衝擊波向四方的空間傳散開去。在附近的魔兵們如同塵埃那般被吹走，站在防壁上的Wood Elf（樹精靈）們也失去平衡的跪下。

緊接衝擊波來的是劇烈衝擊時產生的大音量。那不是普通的音量可言。

隨著蓋歐魯多的騎槍突破了厚重的樹木防壁，幾乎同時的構成防壁的樹木的悲鳴打向我們全身與靈魂。

包含了滿滿的魔界人的殺氣與破壞的慾念的魔力，造成超出樹木物理承受的範圍打從靈魂感到痛苦，聽到那聲音的克里斯汀娜和賽莉娜一瞬間全身貫穿般疼痛而倒吸一口氣。

人類和拉米婭都感應到這樣了，更別說與森林共存亡的Wood Elf（樹精靈）所感到的痛苦與疼痛，那是無法用語言形容出來的吧。

基歐、菲歐、瑪露和其它的Wood Elf（樹精靈）當場單膝跪下咬緊牙根，拚命的忍耐身心所感受到樹木的痛楚。

基歐他們端正的容貌汗如雨下，原本就白皙的肌膚像失去血色般變得蒼白。

不過一度停下的腳步再次動起來了，他們的心中開始激烈燃燒起來。

那是對傷害了可以說是自己家人的樹木們、為生活於森林者們帶來豪不講理的死亡的魔界者們的純粹的憤怒。

我沒看錯那在基歐猛禽般的眼中猛烈燃燒著甚至要燒焦自身的怒火。

在我們視線面前的是蓋歐魯多把貫穿防壁至末端的騎槍拔出，四只獸腳為了下一波的突擊而開始準備移動了。

「汗昂（Hann），就算能忍耐魔兵們的攻擊卻耐不住吾的槍的一擊嗎。剛才那悲鳴還真是令人大快人心啊。怎樣，給吾再吃吾槍一擊如何」

至今為止暴露在魔兵的猛攻下也可以無傷為傲的樹木防壁，現在被殘酷的開了個大洞。

看著那被騎槍一擊就斷絕的樹木與長春藤的斷面在蠕動，以平常意想不到的速度再生著，但是看來蓋歐魯多再追加一擊的速度要快上許多吶。

蓋歐魯多想到再度突擊很麻煩，乾脆瞄準因剛才的一擊在防壁上開了的大洞，自己的右腕往後拉，企圖把洞弄得更廣大。

不妙，正當我打算用大招砸向蓋歐魯多，正想從龍種的靈魂擠出魔力那一瞬間，蓋歐魯多一步踏下之處，從地面的裂縫同時伸展大量的荊棘，纏繞著蓋歐魯多魁梧的身軀，將之向空中舉起。

「賣弄小聰明，這種程度的束縛就想纏住本蓋歐魯多……嗎啊啊！？」

纏住蓋歐魯多魁梧身軀的荊棘，跟蓋歐魯多先前踏毀的荊棘不同，發揮了讓血色鎧甲和獸的下半身轉身以外的動作以外都不容許的拘束力。

生長得密密麻麻的刺死死地咬進蓋歐魯多的下半身，從被刺貫穿的地方開始滲出像沒有星座的夜晚般漆黑的血液。

荊棘開始貪婪的吸食起那的黑血和魔力，緊接著從荊棘的各處開始盛開薔薇。

那是令人想到不論是任何光芒都無法觸及到的深淵中那深邃漆黑的黑薔薇。

「咕噢噢噢，你這傢伙——！吸食吾血和魔力盛開嗎！？區區的忌諱黑薔薇！！」

不顧一切地胡亂揮動騎槍右腕，蓋歐魯多正因無論如何也無法解開這黑薔薇的拘束而暴動著。

可是就算蓋歐魯多再怎麼暴動，黑薔薇的拘束也一點都沒舒緩，倒不如說越是暴動荊棘便更有力的咬進蓋歐魯多的身體，更加肆無忌憚地大量啜食著漆黑之血。

驀然，蓋歐魯多的正對面位置的防壁上，出現了新的人影立於那。

對即使是遠眺也是婀娜多姿的人影真面目，潸然的月光扒下夜暗的服飾，罪孽深重地將之揭露開來。

是女性。只不過，必須要用美人，或者是傾國傾城來形容。然後，盡是妖艷。

看著被黑薔薇纏住，荊棘捕捉住的蓋歐魯多那如黑玉般的眼鏡，發出好像要燒光焦點的殘酷光芒，那是猶如在極北大地吹起的吹雪般冰冷的目光。

優雅的站在防壁上其女性肢體可說是理想形之一甚至可說更勻稱，豐滿突出的玉兔和圓潤曲線的臀部，連接著的纖細的小蠻腰比起克里斯汀娜有過之而無不及。

她穿著清晰地展現出其妖艷的肢體線條的漆黑禮服（Dress）。

一直延伸到大腿根部的深開叉在禮服的左側，從其間隙伸出一條被薄薄的長筒襪（Stockings）包裹住的白皙玉足。

尤其為禮服的上半份，是從將勾勒有極度吸引男性目光的罪惡深重深谷的雙峰的上半分，到纖弱的柔肩為止一直開著的大膽設計。

宛若黑玉的璀璨黑髮中，混有著比拘束蓋歐魯多的還要纖細的荊棘，瀑布般的黑髮到處都綻放著漆黑花瓣的黑薔薇。

更是在兩耳上的附近的各有一朵大的黑薔薇。

我立即知曉了這個美女的真面目。

「薔薇，不，黑薔薇精麼」

「是得嘶。是恩特之森的薔薇精裡，最強的迪婭多菈得嘶喲！」【ディアドラ】

對我的低喃，理應處於被恐怖與痛苦襲擊的瑪露用情緒高漲的聲音回答道。能讓瑪露這樣出聲的，看來對那黑薔薇精很值得信賴吧。

「迪婭多菈麼」

冷酷地俯視著被黑血潤濕的魔界者，背對著皓月作為夜晚的女王君臨的迪婭多菈，其威嚴簡直就是王者之風範。

有著冰雪的眼神俯視著蓋歐魯多的迪婭多菈那型姿，令我感覺到了初見見到賽莉娜時同樣的心動。
