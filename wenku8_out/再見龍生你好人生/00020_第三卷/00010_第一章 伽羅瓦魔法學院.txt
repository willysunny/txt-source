即便不是發自本願，但我和在貝倫村裡受我照顧的拉米婭美少女賽莉娜也還是一同以被總督府的管理官高達半強制性押送走般的形式造訪了近鄰的大都市伽羅瓦。然而實際上，這次的訪問之中含有著魔法學院學院長歐理維爾和高達他們的打算，最後情況變為了我們被請求配合他們捕捉在總督府內暗中活動的祁蓮。
不過，通過這次事件我也有所收獲。這次事件使得我重新認識到了貝倫村的生活是在国家、或是說政治這一人類社會的體制上所成立的。並認為自己有必要去獲得一定程度的社會地位，以此來從如同祁蓮一般的心懷惡意的權力者手中保護好貝倫村。
雖然只是一次小事件，卻成為了曾認為過以普通農民身份終了一生也不會後悔的我的大轉變契機。

⋯⋯
⋯⋯

上述的經歷確實有著，我和賽莉娜在伽羅瓦逗留了數日，今天早上終於開始回村子了。
我和賽莉娜背對著歐理維爾給我們準備的馬車，同前來為我們送行的各位打著招呼。
十分不捨得和我們分別、並非此世間之物的美貌上淨是濃郁憂傷之色的是銀髮女劍士克里斯汀娜。利用魔法學院春季的長期休假逗留在貝倫村的她，還是在捕獲祁蓮之際因擔憂我們的安危而急忙趕過來，單手握著愛劍艾祿斯帕達親自上陣大殺四方來幫助我們的恩人。
雖然相處的日子很短，但似乎是多虧了度過的時間非常緊密之福，克里斯汀娜對我和賽莉娜抱有了深厚的親愛之情。
當然，我和賽莉娜也變得相同程度地喜歡克里斯汀娜了。

「雖然最後的最後被卷入了意料之外的事件中了，但是造訪貝倫村以及和你們的相會都是我一生的珍寶吶」

克里斯汀娜看上去有些害羞，同時也是一副非常自豪的模樣對我們說道。呼呼，被如此坦率地說了，就連這邊也變得不好意思起來了吶。

「那樣說的話，我們也是一樣的。不過，克里斯汀娜桑來到村子的時候村長他們似乎很開心，個中理由，希望總有一天能夠從克里斯汀娜桑的口中聽到」
「這樣啊。說得是吶，多蘭的話，也許總有一天會跟你說的吧。而且如果你入學了魔法學院的話就是我的後輩了，還會因為什麼而見面的吧」

現在不過是一介農民而已的我所選擇的獲取社會地位的方法是，入學克里斯汀娜所就讀的魔法學院。
魔力量至少能成為魔法使的人可以說是百里出一，並且「這些人」被重用著，而在伽羅瓦魔法學院裡聚集著的人們即便是在「這些人」中也是將來有望的存在，如果成為這樣子的伽羅瓦魔法學院的畢業生的話，就跟鍍上了一層黃金是一樣的。不僅僅是總督府，來自各地貴族、大商人的邀請也不會少的吧。
基於歐理維爾和高達的建議，我決定了要入學魔法學院的事也傳達給了克里斯汀娜。
但是克里斯汀娜卻是一副我已經通過了入學考試般的口吻。身為學院長的歐理維爾也很直白地說出了歡迎我入學的言語，但是，我到底會接受怎樣的考試啊。當然，我的目標僅有合格而已。
克里斯汀娜也沒有忘記對賽莉娜的關心，說道。

「賽莉娜，這一次的事件對你來說真的是非常蠻不講理的災難。至少並沒受傷還算是幸運，但身為友人我真的很擔心你」

我聽到這一言語，隨著開心一起浮現出細微的笑容。
畢竟貴族千金大小姐的克里斯汀娜極其自然地稱呼因身為魔物而被人忌避的賽莉娜為「友人」了。雖然說出口的克里斯汀娜以及被稱呼的賽莉娜都未察覺到，但這可是不明曉我們關係以及情況之人聽到後會懷疑自己耳朵是不是壊了的台詞。不過，我們中沒有一個人覺得奇怪。

「我沒事的。多蘭桑一直陪在我身邊，而且克里斯汀娜桑也用這邊都害怕的樣子來幫助了我們」

確實跟賽莉娜說得一樣，克里斯汀娜闖進祁蓮公館時的拚命神色，同她那副不應此世存的美貌相結合後，僅可以用駭人來形容。那時候的賽莉娜也許真的覺得很可怕。
話又說回來，賽莉娜說出了何等惹人怜愛的話啊。若是我總算有所鼓舞了賽莉娜的話，這就是我最高的幸福了。我的心中稍稍有些溫暖了起來。

「呼呼，真羨慕啊。賽莉娜若是有多蘭在身邊的話就沒事這麼一回事嗎。只是，多蘭入學魔法學院了的話，我覺得你們兩位在一起是比較困難的，真的已經好好考慮好了吶？」

言外之意則是『如果你做了讓賽莉娜傷心的事情的話，小心吃不了兜著走吶』

我向克里斯汀娜點了點頭回答道。

「當然方法已經想好了。也已經取得了賽莉娜的同意，第一步暫時搞定了」

讓賽莉娜傷心了一事，我大概是比克里斯汀娜更加不允許發生的吧。

「這樣啊，這樣子的話就好。多蘭的話，應該是不會強迫賽莉娜做不願意的事情的吧。雖然很是不捨，但也就此別過了。挽留你們太久的話，會讓貝倫村的大家擔心的，我這一段時間裡就忍耐著吧。我期待著你們兩人穿過魔法學院的大門。我這可是在說真的哦？」

克里斯汀娜雖然于長期休假中逗留在了貝倫村，但現在假期已經所剩不多了，因此，雖然很是遺憾，但她也只能這樣子留在伽羅瓦了。

「就算不那麼叮囑，我也相信你講的。畢竟我和賽莉娜都十分期待著和克里斯汀娜桑的再次見面吶」
「嗯。就是多蘭桑說的這樣子，克里斯汀娜桑」

我們都確信這是一次短暫的分別，笑著說出後會有期。沒有悲壯感，僅僅有著些許的寂寥以及眾多的親愛之情在其中。

†

我和賽莉娜毫髮無損地回到村子時，得到了髮小阿魯巴托和神官淥剃莎他們以村長為首的村中總動員迎接。父親和母親都發出歡喜的聲音，因我們的平安無事而大感開心。
因為被封嘴了，所以沒法說在被帶走後發生了什麼，但是村子的大家似乎都察覺到了裡面的情況，並沒有深加追問。大概村子的大家都很明白「不知道為好的事情不去知曉方才是聰明之舉」吧。
倒是我跟父母說了有要商量的事情，於第二天往老家走去。
我的老家裡有父親母親和兄長夫婦以及弟弟麥爾卡五人住著。
父親格拉龍是一名用在殘酷的邊境生活中鍛鍊出來的身體在田中躬耕、狩獵栖居平原的鳥獸、反殺襲擊村子的野盜或魔物的男子。雖然相當的沉默寡言，但是他那傷痕累累的肌肉後背比言語更有力地告訴了幼時的我們，「在邊境的生活是怎麼一回事」、「所謂的男人應該怎樣去做」

相對比，母親艾露瑟娜的表情很是豐富，性格上則是相當地喜歡交談。在淨是辛苦的邊境生活中，母親也一直是笑容滿面地照顧著丈夫和孩子們，在這一位面前，我這一生怕是都抬不起頭來的吧。
而迎接造訪老家的我，也正是面容上浮現著一直以來的熟悉笑容的母親。
在桌子上已經擺上開瓶了的麥酒以及葡萄酒，我在椅子上坐下後，父親向我遞過來到倒滿了赤黑色葡萄酒的木杯。

「來了啊，多蘭。你這傢伙有事商量還真的是少見，不過總之先幹了再說」

剛一同桌就被勸酒，還真是不知說什麼好。仔細看的話，兄長諦嵐和弟弟麥爾卡都因為父親一如既往的模樣而微微笑著。
父親的表情變化就我出生以來所見到的，用兩只手都數得過來，我從這樣的父親手中接下杯子，感覺自己的嘴角也有所鬆緩。

「那，我就恭敬不如從命了」

我將微微酸味的葡萄酒移到嘴邊，就這樣子一口氣全部喝光。
多虧了酒鬼父親的血脈，我的身體對酒精也有著相當高的耐性。到葡萄酒通過咽喉流落入胃中的感覺結束後，我把杯子放到了桌子上。

「幹得不錯。雖然諦嵐也是這樣，但你喝酒也挺能幹的吶」

比說話還早，父親立刻就把我空掉的酒杯再次倒滿葡萄酒。

「畢竟我可是爸的兒子吶」

對我極奇自然地說出口的話語，父親岩石雕像一般的容顏的嘴角僅少許地鬆緩了。這是父親那即便是身為家人的我們也不知道何時能看到的罕見微笑。

「是嘛」
「是啊」

不一會後，母親和嫂嫂瀾姐就端過來了料理，除了未成年的麥爾卡以外，我們再次開始了傾杯。
我找親人有事商量確實是很罕見的，但父親和兄長看上去似乎已經稍稍地猜測到了商量內容的樣子。

（果然是因為是血脈相連的親人嗎？）

這樣想到後，不知為何，我感覺到很開心。
因為同親人對酌的氛圍太過心舒，我不知不覺地遲遲沒有開口，但也不能就這樣光喝酒給結束掉。在喝空了好幾個酒瓶時，我終於開始說出要商量的事情。

「今天來商量，是因為想到要不要試著離開村子一段時間」
「這樣啊」

父親一口喝盡他喝習慣的麥酒後，用和平日裡一樣的平穩語調回答道。
代替僅嘟囔了一言的父親，諦嵐兄長皺著眉頭向我詢問到。

「你說離開村子，是要去伽羅瓦當自由勞動者嗎？還是說打算當商人？不過，如果你說要去當冒険者的話，我不大贊同吶，太危險了」
「哪一個都是有著各自的誘惑的，但是⋯⋯其實我是在想要不要接受魔法學院的入學。根據成績排名應該是可以得到學費的免除。不過首先得要通過了入學考試後再說就是了呢」
「魔法學院啊。和丹澤爾桑走同樣的路嗎⋯⋯」

諦嵐兄長皺成川字的眉頭稍稍鬆緩了些許。
與父親同年代的丹澤爾桑，既是我魔法師傅瑪古爾婆婆的兒子，現在也正于伽羅瓦魔法學院里手執教鞭。他是我們村子走出去的人中間最為成功的人。
在我同歐理維爾碰見之前，丹澤爾桑就好幾次試探過我入學一事。
因為至今為止都不想離開村子，所以雖然覺得很抱歉，但也還是一直拒絶著他，不過已經堅定了入學想法的現在，話就得另說了。

「倒也沒打算完全和丹澤爾桑走同一條路，不過我必須得要離開村子一段時間才行，這一點是不會變的。我打算明天在瑪古爾婆婆那取得聯絡後，就去接受入學考試」
「嘿誒，哥哥你明明到現在為止一次都沒有說過要離開村子吶」

發出咯吱咯吱聲吃著炒豆的麥爾卡一副打自心底的不可思議表情。
我這個弟弟無視自身女性一般的纖細外表所做出行為舉止，其中的粗枝大葉之處真的很是顯眼。或者說，這也有可能是他為了主張自己是男性而故意擺出來的模樣。畢竟他也有那般程度地在意自己說成是美少女也可以通用的外表吶。

「麥爾卡，這個跟你也是有關係的哦。畢竟我要是入學了魔法學院的話，讀書期間沒人管的房子跟田都想要交給你來照顧。在學院裡畢業後，根據情況我打算就那樣子轉讓給你」
「誒誒！？可是我一個人生活是明年的事情去了。就算說的是哥哥你已經入學了魔法學院的話，也要早了一年啊」
「房子跟田總不能放在那不管。就算是提早一年也不是什麼困擾的事情對吧。村長也不會擺出一張難辦的臉」

從麥爾卡的角度去看，我覺得入手了自己熟悉的田地應該還不壊。不過，也有可能會覺得已經經由過了我手到某種程度了的房子和田地就跟長輩所給的舊衣服一樣，而心中有所不滿。再加上，他大概還沒有做好離開家中的心理準備吧。

「嘛啊，不會勉強你的啦。你不接受的話，讓村長其他安排人管理吧，大不了就是丟在那不管，也沒事的。等我回到村子後再整理就行了」
「嗚～嗯，但是哥哥耕耘得那麼好的田就這麼給荒掉也太浪費了吶。不過，魔法藥材料的照料我還是真做不到的呦。那個得哥哥你自己來處理掉，或者拜託瑪古爾奶奶她們照料吶」
「昂。我打算帶一部分去魔法學院，大部分的就全寄存在瑪古爾婆婆家裡。就是明明還沒有接受考試，是不是有點性急了啊」
「你這傢伙的話，肯定會跟平常一個模樣地來報告合格了吧啊」

諦嵐兄長語氣有些麻木地說道。大概是我的前科所導致的這一語氣吧，畢竟我從以前開始對解決困難就是一副看上去沒什麼大不了般的樣子，而且也確實實踐完了。
一直側耳傾聽著我們意見、基本都沒開過口的父親用總結完了自己的考慮且不容分說的語調跟我們這麼說道。

「多蘭，你從以前開始就一直思考著一般的話是不會去想的事情，並且還實行了。
我常常會想自己真是生了個有夠怪的孩子，但同時我也知道你一直都把我們這些家人和村子的大家放在第一位去考慮。如果這是你完全考慮好了後的決定的話，我什麼也不會說。
再說，你已經是從這個家裡出去自己獨自成家了的出色大人了吶。
只是，有一點我得問問。你想到要離開村子，原因果然是出在這段時間的管理官那一件事情上嗎？」
「昂，就是爸說的那樣。因為那件事，我體現到了外面的世界給村子帶來的影響是怎樣的東西了。所以我就想到，要是管理貝倫村的人的一個想法就能大幅度地左右我們的生活的話，那麼我就成為那個管理人吧」

諦嵐兄長跟麥爾卡似乎都沒有想到我是考慮到這一點後才打算離開村子的。成為管理官──也就是以官員或貴族為目標，兩人因為我的這一告白而出現一副吃驚的表情。
於兄弟未察之中，便已完全看穿我之所思，應當說果真不愧為父嗎？

「你一直都是想到了就立刻去做的吶。要是麥爾卡說他要當貴族了的話，我就一拳頭把他從夢裡面給打醒過來，但你的話⋯⋯」
「老爸，多蘭哥哥跟我的待遇是不是差太多了啊？」

麥爾卡撅起下唇以示不滿地跟父親抗議道，但父親把那完完全全地給無視掉，繼續說道。

「你被瑪古爾婆婆看上，還被充分地教導了魔法，雖說是在這樣子的邊境村子裡面，但也沒人的腦子能比你轉得更快了吧。
以前你就是個讓人覺得不可思議、認為會做出點什麼的孩子，現在就是那個時候了吧。所以，你按你自己想的去做就行。
只是別忘了吶。雖然你離開了家，但對我來說也是個無可替代的孩子！如果感到難受或者迷茫了，隨時都可以回到這裡來。
我也好，艾露瑟娜也好，諦嵐也還，瀾也好，麥爾卡也好，一直都會歡迎你的」

我因父親的言語，一時之間無法順利地找出言語來說出口。

──家人嗎。

雖然生為竜時並非沒有和其他的原始七竜們互相稱呼為家人，但果然還是得說，重生為人類後的家人跟那個完全不一樣。
當然，對我來說同為原始七竜的Leviathan（利維坦）和Behemoth（巴哈姆特）他們也是重要的存在，但是在愛怜這一含義上的話，我對現在的人類家人所抱有的感情要強烈很多。
看著什麼也沒能說出來的我，麥爾卡笑眯眯地浮現出觸怒這邊的笑容。

「怎麼了怎麼了，多蘭哥哥，害羞了嗎？」
「Fumu⋯確實是那樣，但你小子的樣子總感覺來氣吶，麥爾卡」

我輕輕戳了一下笑眯眯笑著的麥爾卡的腦袋。這傢伙的壊毛病就是特容易得意忘形。

「疼！沒有打人的必要吧？」
「這是同不尊敬兄長的弟弟相應的回報。再說，我有手下留情了。跟爸的拳頭比起來的話，能有百分之一就不錯了吧。真是的，我還是太嫩了吶」

麥爾卡苦著一張臭臉看著像是故意說的我，大驚小作地摸著被戳了的腦袋。
我斥責愛開玩笑的麥爾卡是我們兄弟之間反覆重複的情景。如果入學了魔法學院了的話，這樣的對話大概有一段時間是再也不會有了的吧。念此，寂寞之感油然而生。

「那種事情不是由自己說的啊。噢—、疼疼疼」

我注視著絲毫不隱藏不滿地說道的麥爾卡，此時我的眼睛裡定然比以往都要充滿慈愛的吧。
在這之後，我跟母親還有瀾姐聊起離開村子後的事情（打算），豪飲著酒，大吃飯菜，高聲歡唱，這一天我就這樣子留在老家。在生我養我的家中度過的時間以及回憶，令得我想要成為家人和村中大家的支撐的想法愈發強烈。

⋯⋯

翌日清晨，離開老家的我立馬去造訪瑪古爾婆婆的家中，聯絡身在伽羅瓦的丹澤爾桑，拜託他幫我搞定入學魔法學院必要的手續。
要是在伽羅瓦的時候就跟丹澤爾桑取得聯繫了的話就好了，但是當時的狀態跟被半監禁了差不多，想同外部取得聯繫⋯⋯還是算了。
幸好很久之前就有著丹澤爾桑推薦的我並不需要支付入學考試必要的考試費用，不需要開始動從高達那得到的封口費。雖然並不是支付不起的金額，但並不僅僅是魔法，大概還教導民間高等教育的魔法學院的考試費用，從平民的收入來看還是相當高額的吧。我在心中因被免除了剁手級別的費用而大呼了一口安心氣。
是在我聯絡之前也從歐理維爾那聽說過了麼？丹澤爾桑在我聯繫後，立刻就乘著製作樸素、重視實用性的茶色轎馬車回來村子了。
在丹澤爾桑到達村子的這天，通過瑪古爾婆婆的黑貓使魔得到傳呼的我立刻朝著瑪古爾婆婆家裡走去。

「瑪古爾婆婆，我多蘭」

一入門的大房間裡放置著一張桌子，瑪古爾婆婆深深地坐在桌子旁邊的椅子上，目光朝向著我。
地方病、魔物、自然災害、異民族侵攻⋯⋯在這片邊境裡威脅生命之物每天都以各種形式襲擊而來，而在這樣的邊境裡持續保護了眾人命脈的魔法醫師的眼睛雖然深深埋入了皺紋之中，但是老齡時到來的衰弱卻一丁點兒也沒有，反而寄宿著富有知性以及能看透人心般的光芒。

「不好意思啊，多蘭。這不肖子總算回來了，所以至少得快一點喊你來」

隔著桌子站在瑪古爾婆婆正對面的丹澤爾桑同我最後一次看到他的時候沒什麼不同，依舊還是一副硬朗的模樣。嘛，雖然說是這麼說，但實際上也沒有隔上太久，畢竟丹澤爾桑在魔法學院的長期休假或是尊公辰忌時會回來村子，加上我也從師于瑪古爾婆婆，每年都會和他見上數天。
丹澤爾桑下顎尖以及鼻子同嘴唇之間的鬍鬚都打理得很好，身上穿著以金絲刺綉鑲邊的披肩，黃金雕為頭飾的手杖配上他身上的衣物，站在那兒就有著一種紳士的風格和氣質。

「好久不見了吶，多蘭。你總算肯回應我的邀請了，我很高興啊」

丹澤爾桑如同鎖定了獵物的猛禽一般的尖銳眼瞳在倒映著我模樣的時候，稍許地柔和了下去。

「自己想了很多方面，決定試著抱有稍微跟自己身高不相稱的野心了」

對正站著說話的我和丹澤爾桑，瑪古爾婆婆招了招手，讓我們坐到椅子上去。

「兩個人，都來這裡坐下。現在，就給你們泡茶吶」

在我入座的同時，丹澤爾桑也坐在了椅子上，沒過多久，瑪古爾婆婆就準備好了三人分的藍色水面茶。

「今天的主角是多蘭。老婆子不會插嘴的，但對話而已還是能聽一聽的吧」

瑪古爾婆婆喝起自己親自泡好的茶，並表明了她不會參加我和丹澤爾桑對話的立場。
我雖然是瑪古爾婆婆的弟子，但關於魔法這一塊已經得到了秘訣皆傳的認同，而且我也已經是成人了，似乎瑪古爾婆婆不論是身為師傅還是身為長輩，都不打算插手我決定好了的事情。
在一口飲盡瑪古爾婆婆所泡的藍茶後，丹澤爾桑單刀直入地切入了主題。

「那麼就立刻開始說吧，關於入學我們伽羅瓦魔法學院一事⋯⋯」

對調整好姿勢的我，丹澤爾桑開始緩緩說道。

「學院現在已經結束了入學儀式和進級儀式。不過，若是找到了有資質者，並且本人也希望入學的話，對此表示歡迎便是魔法學院創立以來的老規矩。
當然，並不是說規則寬鬆到只要有資質就一定能夠入學。需要你前往學院裡一趟，接受筆試和實技考試以及面試來向學校證明你的能力。
話雖這麼說，以前就有過我的推薦，再加上學院長自己也在恩特之森的事件裡看到過你的表現了，不合格是首先不存在的」

我聽說過由於持有魔法資質的人類是絶對性稀少的原因，若是找到了的話，都會被積極地勸誘入學。事實上也如丹澤爾桑所言，若是有著教師推薦的話，注定是能夠入學的吧。

「話又說回來，多蘭，我在學院長從故鄉回來了的時候聽到她說跟你碰見了，而且你更是還幫忙了擊退魔界者侵略的時候，感覺心臟都快從喉嚨裡蹦出來了吶。
雖然只是遇上了小規模的軍隊侵攻，但不也是在森林居民的緊要關頭處趕到了，並漂亮地大活躍了一番嘛。
我清楚你的魔法才能非同尋常，以前也覺得就算是面對魔物或者蠻族，你也會不知道什麼是怕地打上去，但還真是沒想到你居然能跟魔界者們打吶。看樣子，我對你的評價似乎過低了」
「那個是因為和森林的戰士們一起戰鬥才做到的，我不過是搭了一下手而已。
話說起來，丹澤爾桑，您知道一位叫做克里斯汀娜的女學生嗎？有著長銀髮和鮮艷的赤紅之瞳，是一位非常漂亮的高個子女性。
她也跟我們在恩特之森裡一起戰鬥了。看上去跟歐理維爾學院長互相認識的樣子，就想說不定丹澤爾桑也有可能知道？」

在我說出克里斯汀娜的名字時，傾聽著我們對話的瑪古爾婆婆似乎些微地抖了一下肩膀。
雖然是血脈相連的丹澤爾桑都未能察覺到的細微反應，但這是最為雄辯的說明──瑪古爾婆婆跟村長一樣，也知道克里斯汀娜的身份。
丹澤爾桑的反應則更為明顯。他微皺著眉頭，戴著黑手套的左手摸著顎須，一副看上去正在頭疼著該怎麼說的表情。

「克里斯汀娜嗎，她即使在伽羅瓦魔法學院裡也是相當有名的名人啊。嘛啊，那什麼，就是有著各種情況的學生吶。
她本人非常認真且才能眾多（出眾），實在是一名相當優秀的學生。你跟她也有結緣，這也是意料之外啊。
你從克里斯汀娜那聽到她自己的事情到哪一程度了？她的情況需要稍微挑選一下所知者。關於這一點她自己比其他所有人都要清楚，應該不會隨便地就到處宣揚的」
「姑且，也算是一起在豁出性命的戰鬥中並肩到底的同伴，我想我們的關係應該比一般要親密。說是這麼說，但大部分情況都沒有聽到。
並不是一出生就是過著貴族生活，幼時在国內各地都旅行過，母親已經去世了，以及跟現在的家人相處得並不是很順利的樣子。我所知道的大概就是這麼一些。還有就是跟剛來村子那會比起來，她的表情似乎變得開朗了的樣子，其他就⋯⋯」
「這樣啊。嗚姆，嘛啊，那種程度的話沒什麼問題吧。不過考慮到她在魔法學院裡的生活態度，你跟她還真有夠相處融洽的吶。
克里斯汀娜因為那份美貌和出類拔萃的實力而在學院的學生中很有人氣，但是並沒有什麼知心的校友。原本身上就有著一股他人勿近的氣場，她自己也有意地跟他人之間製作著牆壁。
如果你入學了的話，能幫忙關心一下她就太感謝了。本來這就是我個人的願望⋯⋯」

話雖如此，克里斯汀娜原來在教師看來，也是被認為朋友很少嗎⋯⋯以她本人的性格為參考的話，大概是她自己想那麼做的，但家裡的事真的就那麼沉重嗎？

「那也是我希望的。畢竟我跟克里斯汀娜桑不可思議地很投緣呢」
「能這麼說就太好了。關於考試的事情，可能的話，我想你在這之後跟我一起去魔法學院，快一點接受考試。
雖然基本上沒有備試的時間了，不過你的實力的話，應該不會有問題的。你肯定想有充足的時間去應對，但跟進級儀式不隔太久，你比較容易跟其他的學生打成一片吧。狀況如何？」
「Fumu⋯沒問題，但有沒有什麼必須要準備的東西吶？」
「筆試的用具我這邊來準備。當然，拿平常用慣了的去也可以，但實技考試中的使用的魔杖等都規定了只能使用由學校準備的，所以你平常用的魔杖或長劍都不能用。
還有的話就是，要準備去伽羅瓦途中的替換衣物和護身用的武器吧。那，還有其他什麼想要問的不？」
「沒了，就是我要是更早一點表示要入學的話就好了這一點，還請您不要介意。令就是姑且想要確認一下，我入學了魔法學院之後，就得住進宿舍裡面了對吧」
「昂。規定上是說學院的學生要住進宿舍裡面。從中等部入學的話，到高等部畢業一般需要花上六年時間。
不過從你的學力和魔法實力來考慮的話，至少是高等部的一年級生，說不好的話進入二年級大概也是很有可能的吧。
你這傢伙至今為止都以會離開村子為理由不肯入學，所以入學有點晚，但根據成績和成就也可以跳級。得通過你的努力就是了吶」
『順利的話，一年時間也能夠畢業』這麼一回事嗎？不得不從村子裡離開這一現實，即便是決定要入學了的現在也依舊在心裡是個疙瘩，所以這對我來說是十分幸運的話題。
把丹澤爾桑的話簡單地總結一下吧。
魔法學院的規定，依據入學考試的成績會得到免除學費或獎學金授予的待遇，就算之後的成績有所波動，這些待遇也不會變。再就是，我的學力的話是從二年級左右開始入學，依據成績，能夠在成為三年級之前就跳級完成畢業。
如果畢業快的話，畢業後的展望也會大大敞開的吧，更重要的是，我很鐘意根據自身的努力能夠大幅度改變結果這一點。
萬事皆由自身努力去開闢未來，這挺合我口味的。
然後，我還問了丹澤爾桑一個最最重要的事情。

「丹澤爾桑，我和現在住在村子的拉米婭少女很是親近的說⋯⋯」
「嗯？」
「其實我跟她說了我要離開貝倫村後，她說她也想跟我一起去伽羅瓦。我在考慮用以前從您這請教到的方法，帶她一起去伽羅瓦」

丹澤爾桑對我的話暫時開始瞑目起來。為了在腦中總結好對我的回答，似乎必須要花上一定的時間。
我一邊回憶著賽莉娜在我說要離開貝倫村時我所看到的悲傷和寂寞，以及在決定一起同行時我所看到的燦爛無比的美麗笑容，一邊等待著丹澤爾桑的回答。

「雖然我知道經過你的中介，有拉米婭開始生活在村子裡了，但你們的關係居然好到希望跟你一起去魔法學院，還真是想像不到吶，多蘭」
「她叫賽莉娜。畢竟我有把她帶到村子裡住的責任，而且說實在的，比起一個人去魔法學院，兩人一起感覺比較膽壯」
「你這傢伙的神經才沒有那麼纖細。那個叫賽莉娜的拉米婭並不危險這一點，我也從村子裡大家的話裡面充分地明白了。
但是拉米婭是時常襲擊亞人種的魔物，人類更是她們的首要目標。別說魔法學院了，光是進入伽羅瓦就不是什麼容易的事。
這種情況下把那個叫賽莉娜的拉米婭變成你的使魔啊⋯⋯如果是使魔的話，即便是危險的魔物或猛獸也可以帶著一起在魔法學院或伽羅瓦裡面走，這一點你似乎是記得很清楚吶」

我在瑪古爾婆婆的授課中得知，對這個時代的魔法使們來說，使魔似乎並不僅限於小動物、猛獸或魔物，自己製作的魔法生物或者人造人也是可以的。只是，把人類或亞人弄成使魔一事出於倫理面是堅決禁止的。
使魔化後的動物或魔物會有一部分精神跟其主相連，能夠不依賴言語，以思念來完成互相理解以及五官共有。使魔的知性和魔力越強，其主也就有著能得到使魔所擁有的記憶、知識或生物特性的恩惠。

「如果你無論如何都想要帶賽莉娜一起走的話，就只有把她使魔化這麼一個方法。她是那般程度想要一起去的對象嗎？」
「先前說的就是大部分的理由了。只是，嗯⋯⋯賽莉娜非常的猶見我怜，那樣的她眼淚汪汪地懇求我想要跟我一起去了的話，這個世界上的男性基本上都拒絶不了的吧」
「我還以為那種美人計對你這傢伙完全不通用吶，難道是對普通戀愛有興趣？話又說回來，把拉米婭帶回村子並這麼親近你，多蘭，也許你有著怪物馴獸師的資質吶」

好奇心在眼底深處閃閃發光著的丹澤爾桑，似乎把我看做了比一個人類更加勾引起了他濃厚興致的研究對象一樣。對新魔法藥或是新魔法的開發燃起了熱情時的瑪古爾婆婆，眼睛跟這個非常相似。研究者這一類人，是不是誰都具備著這樣稍許從倫理或道德上脫軌的一面啊？

「如果把拉米婭這樣持有跟人類相差無幾的知性和高魔力的魔物收為了使魔了的話，在入學的時候可以得到更高的評價，不過你的評價已經很高了。而且也快要出發了，沒必要這麼著急地締結使魔契約吧」
「明白了。我等會再去跟賽莉娜商量一下使魔的事情。在入學定下來後，再決定是否跟她締結正式的契約」

丹澤爾桑再次這樣提醒我道。

「對方持有著自己的意志和知性，在使魔契約這一塊，絶對不可以強迫對方吶」
「當然，一切都會尊重賽莉娜的想法」

在一系列對話結束後，丹澤爾桑大嘆了口氣，一口氣把已經冷掉的茶給喝光。這一副看上去總感覺很累了的樣子中，我感覺到不像丹澤爾桑風格的東西，於是詢問道。

「怎麼了？我的入學對丹澤爾桑造成了什麼負擔了嗎？」
「嗚姆⋯雖然只不過是僅此而已的話題，但這事是我相信媽和你的嘴巴牢才說的。我想你們應該是知道的，魔法學院除了王都的本校外，東南西北各還有一校，總共是五所學校。
每所學校都有著各自的獨特風格，包含本校在內的這五所學校其實都互相視對方為競爭對手。因為是同根生的組織，所以在表面上必須得團結，可教師和學生中大多數都要跟他校比較競爭。
最近一段時間，南校和西校都入學了可以說是數十年方有一人的天才吶。加上這個，再跟學生以及設備原本都超出一頭的王都本校，或著跟因為持有東方的交流而獨特風格很強的東校比起來，我們伽羅瓦魔法學院必須得說要慢上一步」
「也就是說，有著跟魔界者的實戰經驗以及瑪古爾婆婆教導的我，是取回那慢了的一步的潛力股？」

偷偷斜瞥了一眼瑪古爾婆婆，婆婆依舊還是一副沒有在聽這邊說話的模樣，但是可以非常清楚地看到刻滿皺紋的面龐上那不滿的神色。似乎是因弟子被綁在了魔法學院的面子問題上，而生氣了的樣子。這是因為在擔心我而生氣的，我心中實在是感激不盡。

「嗚姆⋯畢竟優秀學生輩出的話，會跟學院全體的評價聯繫上，王国下發的預算也會增加吶。實際上，多蘭啊，也許你有著至少能跟西方和南方天才相競爭的能力。你的話，正好能夠成為競爭對手。
而且你也有著我和學院長的推薦，更還有著恩特之森裡的實際戰績，要是更一步把拉米婭收為使魔了的話，就是更加受到宣傳的傢伙了吶」

被這麼一說後想想，我的經歷確實是有夠異常的。對感覺到比他校要慢了的伽羅瓦魔法學院重要級人物來說，我就像是從天而降的好苗子麼⋯⋯

「可是，伽羅瓦裡也有著克里斯汀娜桑那樣優秀的人在對吧」
「嗚姆⋯她啊。確實她的能力即便在學生中也是最尖端，但也有著無可奈何的事情吶。而且她本人的性格又是那麼一個樣子」

丹澤爾桑一副無法言喻的苦瓜臉，從他的言語中得到了克里斯汀娜是毫無疑問的優秀學生的確認，不過嘛⋯⋯她的性格確實不是帶頭受人注目的那種啊。

（Fumu⋯話又說回來，從丹澤爾桑的語氣中來看，跟克里斯汀娜桑一般的優秀學生其他並沒有太多嗎？
要是這樣子的話，我入學的時候以及入學以後是不是能去謀求某些便宜占占啊？）

我的腦子中浮現出這樣的打算。啊呀，我已經完全把人類風格的思考方式給納為己物了啊。

「原來如此，那麼，我就去弄弄出發的準備和招呼就來。對了，您知道締結使魔契約的方法不。這一塊我還沒有跟瑪古爾婆婆學過」
「昂，我和媽當然不用說，妹妹狄娜也是魔法醫師，關於儀式這一塊都很通曉。你在考試結束到接到合格通知的時間裡，跟賽莉娜好好商量使魔契約這一塊的事情就好了。
至於魔法學院裡的使魔定義，途中再仔細跟你說說吧。就算傍晚再出發也趕得上的，不要太著急了吶」

在應要確認的事情都完成後，我從座位上站了起來。

「知道了。瑪古爾婆婆，謝謝款待，茶很好喝」
「是嗎，那樣的話就好。你得好好跟拉米婭的小姑娘和格拉龍他們說好離開的事吶」
「我會的」

撒啊，到達了伽羅瓦魔法學院的話，我的人生就打開了新的大門。然後，在那門後究竟有什麼在等著我呢？
我心中僅有期待及興致，不安之色絲毫不存。

†

「多蘭，有沒有落下的東西？聽好，雖然你是個很冷靜的孩子，但要是緊張了，就反覆地緩慢呼氣吐氣。那樣的話，姑且就會平靜下來了」

母親罕見地浮現出擔心般的表情，手放在站在家門口的我的肩上，反反覆覆地叮囑著我。
今天是跟丹澤爾桑約定好的前往伽羅瓦的日子，而現在則是早晨。
諦嵐兄長和麥爾卡已經出去干農活了，父親跟母親則特地從老家過來送我。
第一次體驗到自己的孩子要接受學院考試的母親，一副我至今為止從未見過的冷靜不下來的樣子。如果我也有孩子了，會不會也像現在的母親一樣擔心個不停吶？
不過父親跟母親完全就是正反面，這邊是一點兒也沒有動搖的樣子。但願我以後也能用父親這樣的態度去對待我總有一天會誕生的孩子們。

「媽，不用這麼擔心啦，只不過是接受考試而已」
「⋯⋯是呢，你從小時候開始就是個不可思議般的不需要操心費力的孩子，也許只有擔心是不需要的啊。而且萬事都應該去嘗試一下，如果沒有合格，也就是跟現在為止一樣生活就可以了。不要在意失敗了時候的事情去加油哦！」

（嗚嗚姆⋯⋯媽啊，想要鼓勵我是好事，但對要去接受考試的兒子連續說出「沒有合格」、「失敗了的時候」這種不吉利的話，是要怎麼搞啊⋯⋯）

隨著和父母的對話交談推進，我連著心中的緊張感一同卸去了肩上力氣，有點打算在考試的時候去取得發揮出十二分努力後的結果。
這個場合，應該並不是母親有意得來的結果吧，但是⋯⋯嘛，就當是受到了父母的恩惠只管向前看吧。
我向目送著我的父母揮手告別，朝著丹澤爾桑等待在的南門走去。
丹澤爾桑所使用的是六匹馬拉的大轎馬車，在光滑的茶色車體上刻印有著司掌魔法與知性的神明奧爾丁的紋章，以及將日月皆擬人化的魔法學院紋章。
車夫是由一位初老的男性來擔任，從他的身上我也感覺到相當程度的魔力，似乎並不是一名單純被魔法學院所僱用了的事務員。
我和丹澤爾桑坐進馬車裡後，馬車在車夫小聲一聲「駕」後開始慢慢地動起來。

「多蘭，到達伽羅瓦這一段時間裡，不打算學習一下嗎？」
「嗯，必須的知識已經全部記住了。以平常心去面對的話，就不會有什麼特大的問題」
「吼哦，能說得這般自信滿滿的人基本上沒幾個人，萬一到了考試的時候想到書到用時方恨少也來不及了呦？」
「筆試只要收納在學習了的範圍內的話，就毫無問題。實技考試也總會有辦法的吧。只是面試這裡不敢說什麼。也就這麼點不安」

如果想要讓我擔任同其他已經先行了一步的學校進行對抗的競爭對手，真的是伽羅瓦魔法學院的打算的話，就算在面試中多少有所失誤大概也是不會落選的，那麼事情會是怎樣發展的啊。

「比起那個來，丹澤爾桑很是關照了我，但要是被看作是學院教師在關照我的話，對丹澤爾桑會不會影響不大好啊？」

我直接問出以前就在意的事情後，丹澤爾桑一邊把玩著嘴邊的鬍鬚，一邊像是在說沒什麼大不了的事情一般對我說道。

「這個你就別操心吶。魔法中也是有著能判斷虛實的，魔法學院那邊會使用這個來判斷我在進行勸誘和推薦時有沒有違反規定的。比如，就算我把考試問題告訴了你，結果還是會在之後被發覺的，所以就算是作弊也沒什麼意義」

在這之後，我再次向丹澤爾桑詢問入學魔法學院之後的事情。雖說是坐馬車，但到達伽羅瓦也是需要時間的。而且教科書和到現在為止的課堂裡的內容全都裝進腦子裡了，也應該用除了學習之外的方式來有意義地使用時間吧。

「丹澤爾桑，伽羅瓦魔法學院現在有多少學生在就讀啊？還有上課是用哪種形式來進行的？」

雖然從丹澤爾桑這得到的書中有介紹魔法學院的，以前開始就知道了大略的情況，但到底還是沒有聽實際執過教鞭的人說過，現在正好是個機會。

「這個嘛，預定把你編入的高等部裡，三個年級往年都是全員在三百人左右吧。雖是學習基礎的學級，但因為其他也會根據個人所希望的選修內容來區分上課，不要太過局限於學級這一框架比較好吧。
成為高等部成員的話，每次考試都會有數名學生達不到學校所要求的水準而留級，不過跟你是無緣的話題吧。雖然大部分的學生都是魔法使一族或貴族子弟，但中間也有著有力商人的親屬以及像你這樣被發現了才能的平民孩子在。後者跟整體比起來數目大概要少很多吧」

在我現在的人生之中，貴族僅知道克里斯汀娜和那個高達，但根據風聞來判斷的話，貴族的名聲一般都不好。
若是回首在前世裡一直看到厭煩了的人類歷史的話，我了解到人類是以身份和出身來差別對待同胞的生物。就算是高聲強調平等並廢除掉了身份制度，結果還是會有其它的理由將己身與他者區分開來，喜好差別對待的本性根本不會改變，也就是所謂的江山易改本性難移吧。
我的話，即便是為了自身和村子今後的展望，也打算盡可能地在學院裡結交不問身份的友人。這一意義上，有著克里斯汀娜這樣的知己在是非常難得的事情。

（嘛啊，雖然很失禮，但克里斯汀娜桑真的是沒什麼朋友的樣子就是了⋯⋯）

不知道我心思的丹澤爾桑繼續講解著魔法學院裡面實行的授課內容。

「高等部以中等部學到的事為基礎，根據各自所選擇的魔法使路線改變，授課也會改變。
思念魔法、精靈魔法、神聖魔法、暗黑魔法、召喚魔法、付與魔法、創造魔法和錬金術。雖然都是魔法，但存在著眾多的體系。
並不僅僅是魔法，基本的書寫和計算是肯定跑不掉的，除此之外有歷史學、政治學、紋章學、經濟學、經營學、商學、藥學、醫學、神學、音學、哲學、文字、數學、語文等列舉起來就沒完沒了的選擇項。
既然你的第一目標是有益於貝倫村的發展，那肯定已經定好了某種程度的方向了吧？」
「嗯，打算以錬金術、付與魔法和創造魔法為中心學習。醫學和藥學已經跟瑪古爾婆婆好好學完了，注意複習就好，其他就是對經濟和買賣也有興趣」
「在學院裡不會強制學習什麼，你按你自己喜歡的去學就好。就是要注意，你得自己認同那些中間存在的價值，值得你作出了離開村子、家人和朋友們這一決斷吶」
「嗯，肯定會的。畢竟將要在學院裡度過的時間，對我來說比黃金和寶石要貴重多了」

我發自心底地說道，這中間沒有絲毫虛假。我離開村子的時間，就算是跳級也最短要一年。
如果連同這段時間相對等的成果都沒有得到的話，我比任何人都無法接受。

⋯⋯

從貝倫村出發了一天左右，我們到達了庫拉烏澤村，首先在這裡停留一晚上過夜。第二天清晨立即便出發，一共花兩天時間就能到達伽羅瓦。
在伽羅瓦和庫拉烏澤村之間有著一條在風吹雨打下磨損了的石板街道，除了我們以外的行人很是稀疏。
到伽羅瓦為止的馬車旅行，我通過詢問丹澤爾桑關於伽羅瓦魔法學院和就學於那的學生們的信息來打發。然後在丹澤爾桑已經全部都跟我說完時，我們也到達了伽羅瓦。

「那麼，多蘭。難得來一趟伽羅瓦，本來是想帶你到街上逛一逛的，但今天最多是來考試的，所以不好意思，直接去學校去了吶」

我跟丹澤爾桑表示明白了，在車內等著馬車到達魔法學院。通過馬車的小窗戶看到的街市普通地充滿了活力，人來人往的。

（Fumu⋯前些日子祁蓮的事情也是，看來我真的是跟伽羅瓦觀光無緣啊）

雖說感覺遺憾，不過要是入學了魔法學院、住進伽羅瓦了的話，逛街的機會要多少有多少。
因為伽羅瓦魔法學院的前身是在伽羅瓦形成之初所創立的學問場所，所以處於只有上流階層者或富裕商人、就任於總督府的官員或上級士兵才被允許居住的街區的第一層城壁和第二層城壁之間。準確位置是在伽羅瓦繁華街區西部，占地面積極廣，周邊圍著有我五倍高的石壁，雕印於正門處的黃金圓盤形魔法學院校徽反射著太陽的光輝。
在沿至正門的白石階正對面有著高達七層的學院宿舍，能從中窺視出來的不曾間斷的歷史流露出一種厚重，宿舍持著這份厚重聳立於那。
考慮到伽羅瓦是為了當做同北方的魔物或蠻族、亞人戰鬥的中心據點，而以城塞都市身份所建築出來的歷史的話，或許魔法學院也存在著戰事中軍事據點的機能吧。
因為入校許可的確認而在學院正門前暫時停下了的馬車再次動了起來，終於進入了學院的占地之內。馬車在從正門開始一直延伸地長道上前行著，當到達主教學樓前時停了下去，讓我們在此下車。
分厚的黑色大理石門沒有發出一絲聲音地打開，引導我們進入教學樓內。
在第一層的是門廳，往頭上看去的話，可以望見吊在遠處頂上絲毫不吝惜使用魔晶石、精靈石和水晶的枝形吊燈。
魔法學院中充滿了寂靜，現在應該也有著數百名人在教學樓裡才對，但是完全感覺不到那份喧囂。
我在丹澤爾桑的帶領下通過位於門廳左邊的一扇門扉後，走在走廊上前進，然後到達了接受考試的房間裡。
雙人用的課桌五張一列，排為三排在教室之中，在教室的前面有著黑板和比學生聽課席位要高一段的講台，此時正有一名應該是擔任考官的教師等在那兒。

「阿利斯塔，這孩子就是接受考試的多蘭。多蘭，這邊是監督你考試的教師。如果在考試中碰到什麼不清楚的，就請問他吧」

在教室講台上等著的是三十五歳左右的男性魔法教師，身上穿著深綠色的袍子，有著細顎、鷹鈎鼻和看似神經質的狐眼。

「我是多蘭，還請多多指教」
「嗚姆⋯看上去不錯。吾輩是阿利斯塔，擔任你這次筆試以及實技考試的監督官。那麼丹澤爾桑老師，能請您離開教室嗎？得快一點開始這個孩子的考試才行」
「我知道了。那麼多蘭，給我不留遺憾地傾盡全力吧」

像是給我打氣一般拍了一下我肩膀後，丹澤爾桑推開教室的門走了出去。

（Fumu⋯我的未來連向哪裡，就在這個地方決定吶）

這樣想到後，就連這間並不怎麼寬敞教室，也給我一種不可思議的感覺以及感慨一樣的東西。嘛，畢竟這裡是我人生中的一個分岔路口吶。
在我感慨的時候，阿利斯塔老師發出了讓我坐下的指示。在講台上放置著計時沙漏和我進行筆試用的紙束。
教室全體施加有阻礙同使魔或傀儡等的精神接續的魔法，通常都是把這個用作對不正當行為的對策吧。
雖然在我看來這一阻礙魔法跟沒有是一樣的，但根本就沒打算作弊，一開始就打算僅用自己的真實水平來突破這場考試。

「那麼請坐在自己的座位上，貝倫村的多蘭。現在就將開始確認你是否有著與魔法學院學生所相應的資質以及能力的考試。筆試的問題是到中等部為止所教學的魔法基礎知識和一般教育。在這個沙漏中的沙礫落盡前，傾盡全力地解答吧。來，拿出筆試用具做好考試的準備」

聽從阿利斯塔老師的話，我從包中取出筆試用具，接過被翻著的問題用紙。
雖然聽過了如果掌握了從瑪古爾婆婆和丹澤爾桑處所受到的教育的話，高等部的入學考試就能夠毫無問題地解答出來，但實際上是怎樣的啊？
阿利斯塔老師看著我做準備後，把沙漏拿在手裡。

「準備好了吶？在考試之中是不允許退席的。東西掉下來的時候由吾輩去撿，注意不要從座位上站起來。雖然不用多說什麼，但在發現不正當行為時，當場便中止考試，你的入學也會變為一張白紙空談。還有什麼要問的嗎？」
「沒有，隨時都可以開始考試」
「很好。那麼，考試開始」

沙漏被倒過來后，藍色的沙礫很爽快地向下方落去。
筆試內容確實是中等部三年級水準，比起我剛開始學習（人類）魔法時所使用的面向初學者的教科書要前進了些許，但也是我在被瑪古爾婆婆收為弟子後沒多久時就學完了的內容。
我握著的羽毛筆毫無停息地動著，當時間還殘有一半的時候就全部解答完畢。
在沙漏中的沙礫全部落盡後，我被阿利斯塔老師帶著，移動向校內的魔法練習場。
在練習場進行的實技考試內容是，有關魔力制御的基礎技術，以及行使被視為基礎魔法、使用在日常中的無屬性魔法。主要是開鎖、上鎖、點燈和熄燈、自身漂浮、用念動來移動遠處的物體等。
之後進行的是使用魔法學院支給的魔杖，單手給魔力枯竭了的魔晶石充入魔力，以及使用精靈石檢查各屬性的適性。
阿利斯塔老師一直無表情地監督著一次失敗都沒有地依次解決掉被準備的課題的我。當然，認為筆試和實技考試都沒有失敗的是我個人，不知道這位考官殿下是怎麼看的啊。

⋯⋯

筆試和實技考試都結束後，我為了接受面試而再次回到了主教學樓。
小憩一會後，在阿利斯塔老師的帶領下走上門廳裡的螺旋階梯，在到了三樓時，停在了一扇同先前接受筆試的教室的門是不同構造的門之前。
閉著的門扉後面有著四道氣息。其中也有著非人類的氣息，這四位應該就是面試的考官們吧。
我仰望站在旁邊的阿利斯塔老師的側臉後，察覺到視線的他俯視著我說道。

「這場面試之後，你的入學考試也就結束了。平時的話，吾輩會說不用那麼緊張，但你完全沒有緊張的樣子吶。嘛啊，算了。要深呼吸不？
準備好了的話，敲門後再進去。進入房間的時候，面試就開始了哦」
「我沒事。非常感謝您筆試和實技考試的監督，您辛苦了」

我在向阿利斯塔老師一禮之後敲了敲門。

『請進』

對房間內傳回來的聲音，我用平日裡的冷靜聲音進行入室的招呼。

「打擾了」

在門扉的後方有著四名背對著掛有白色窗簾的窗戶，坐於長辦公桌前的魔法教師。
坐在四人正中間的是一位老人，身上穿著紅色的袍子，白色長須跟山羊的一樣，長髮也是同鬍鬚一樣的顏色。
第二人是一位四十歳左右女性，身上穿著的是淡紫色禮服，參雜有白色之物的茶髮在腦後束成團子狀。
接著是一位巨漢，厚唇，小眼睛，後者同他那張大臉有稍許地不相襯。看上去根本就不像是魔法使，給人一種用巨岩削出來一般的感覺，都感覺他是我貝倫村的男性一員了。
再接著是身材矮小的陸行族男性。
陸行者即便是成人外表也跟人類小孩子差不多，耳朵稍長，為四角圓潤的四邊形。腳掌分厚的皮上齊長有毛，因為他們赤足能在地面上快速奔跑，所以也能稱之為草原的小人。
看來面試這裡似乎不會有歐理維爾出場，是為了尋求公平嗎？
發出第一聲的是老教師。

「請坐」
「是。我是貝倫村的多蘭。今天還請多多指教」

我步至椅子旁邊，把包放置腳邊一禮後坐下。沒有深坐下，接下來自面試官們的視線。
雖然有種被人看猴的感覺，但這大概也是一種經驗吧。

（Fumu！）

我僅在心中給自己加了口氣，挺起胸膛開始同面試官們的問答。
來自四名面試官的提問中沒有感覺到「尤其是這個」的問題，花了面試相應的時間交談後，老教師告訴了我面試結束。
離去時再次一禮後，我和等在房間外面的阿利斯塔老師會合。合否的通知似乎會在日後傳到村子裡，但面試中也沒有太大的失誤，我想要認為應該是九成九的穩了吧。
清靜地學院走廊中只有我和阿利斯塔老師的腳步聲在以一定的拍子連續響起。
突然，走在我前面的阿利斯塔老師保持著繼續往前走，向我搭話道。

「你真是名有意思的學生。接受筆試、實技考試時的樣子。還有這樣走著的現在也是，腳步聲和呼吸的拍子從來到學院時就沒有變。這種學生吾輩至今為止從未曾見過」
「腳步聲和呼吸嗎？阿利斯塔老師的聽力真的是好呢」
「那麼，那個回答是你的真心話嗎？抑或是在裝乖？」

大概是使用了觸及聽力的觸覺強化來感知空氣或地面的振動，發現了我的呼吸或腳步聲拍子沒有變化一事吧。前者的話是運用了基本技術，使用魔力來增強肉體活性的強化魔法，後者的話是擅長風系統的魔法嗎？

「受到吾輩的指摘亦未曾屏息一會。似乎並不能認為你是同外表所見的學生吶。雖然丹澤爾桑老師也說過，但實際見過後吾輩也能確信了。
言語雖然有些不好聽，但你能夠說是異常。這裡是好的意思呢。你若是入學了的話，定然會被視為對抗南校以及西校的競爭對手，被背負上那種期待的吧。
或許不會是什麼太歡樂的學院生活，但這個根據你的努力要改變多少都可以。除學習以外也有許多要學習的，而且在魔法學院裡你能幹的事情要多少就能有多少的吧」

同西方天才和南方天才幹架似乎會成為我今後學院生活裡的關鍵吶。不論哪一方都是數十年一見的傑出人才，但實際上是怎樣的呢？我心裡早已對此產生了興趣。
繼丹澤爾桑之後，從阿利斯塔老師這也聽到我被魔法學院所需求的理由，要說理所當然也確實能說是理所當然的。
但話雖是這麼說，阿利斯塔老師是不是少許地暴露內部情況太多了吶？就如先前的所言，或許正是因為並未把我視為普通的少年對待才說出來，但在懷著夢想和希望才想要入學魔法學院的我面前說出這樣子的「里之面」，還真不是能誇讚的行為吧。

「對想要在這之後入學的學生言明這樣的情況，您在想著些什麼東西吶」

阿利斯塔老師在聽到我的話語後依舊背對著我繼續說道，如同絲毫不介意一般。

「安心吧。這是覺得你是聽到了也無事的學生才有的發言。這種程度的分辨能力的話，吾輩還是有著的吶」

阿利斯塔老師似乎是位相當難對付的人（敬語），這種意義上那位高等Elf的學院長也是同樣的麼。
說到個性太強而導致太難搞定的對像的話，在眾神之中也是多到厭煩了的程度啊。從同這種強「惡」的傢伙們有著打交道經驗的我來看的話，倒不如說正因為是這種對像才容易打交道，這麼一面是確實存在的。
看來在這所魔法學院裡即將度過的時間，定然能讓我忘記掉侵蝕我生命與心靈的名為「無聊」的最大天敵吧。
在門廳裡面的丹澤爾桑，一副顯得有些冷靜不下來的樣子。

「辛苦了吶，多蘭。嗚姆，看你的樣子，似乎沒有什麼特大的失敗」
「能做到的全都做了。剩下的就只有聽天由命了」

話是這麼說了，可我的話並不能公然地把結果委託給神明。因為我面子太大，而給神明們帶去多餘的操心的可能性是很高的。雖說世界很大，但有這種情況的存在，包含我在內連五指之數都沒有吧。我現在應該祈禱剛才的言語不要給大地母神瑪依拉爾所聽到吶。

「丹澤爾桑，考試好像是結束了，但其他還有什麼必須要做的事情嗎？」

看著既沒有緊張也沒有安心、極為平靜的態度亦為崩潰的我，丹澤爾桑浮現出混有無語在裡面的表情。

「沒了，你今天必須在魔法學院完成的事情這樣就全部結束了。我要就這樣子留下來，但你的話可以現在就回去。回去的馬車我這邊已經安排好了，對方也已經到了正面處，坐那個回去就行。馬車裡面堆著土特產，給我分給村子裡的大家吧」

丹澤爾桑的關照是非常值得感激的，我則坦率地接受了這份好意。

「知道了。非常感謝您的關照，我就滿懷感激地收下了」

看著魔法學院窗戶的對面的話，明明來時還是藍之一色所存在的天空，現在已經染上了橙色。
我向兩位教師道完謝言以及告別言語後，走出了門廳，然後坐上丹澤爾桑安排好的馬車，啟程了歸往貝倫村的歸途。

【第一章End】