她看見了，妖精結界展開的領域在被槍觸碰的一瞬間，好似自己逃跑一樣消失了。唯一的守護之光被消除，被槍刃刺穿是當然的結果。兩次看到同樣消失分解的的莉莉，伴隨著腹部涌上來的劇痛，莉莉明白了。

對妖精的固有魔法進行直接干涉，提高魔法術式進行破壞。
怎麼可能。至少，莉莉不知道是怎麼做到的，從魔法師的常識來考慮是根本不可能的事情。

讓對方使用的魔法被打斷，這種技巧確實存在，比如『閃光』。在發動的瞬間，耀眼的光源炸裂，打斷對手的集中力，從而使對手的術式無法完成，其結果，就是什麼都不會發生。

但這既不是光也不是聲音，除了物理的衝擊以外，什麼都不是。能取消發動前的魔法，只有阻斷對手的集中力，讓其發生一點小意外，並不是什麼困難的事情。
還有，發動中的魔法，也可以通過同樣的方式阻止發動。如果中斷魔法的術式，魔法在被阻礙的瞬間就會強制性失效。

但是，如果對手自身不存在將術式停止的要因的話，想取消魔法是非常困難的。熟練的法師，往往心智也非常強韌，發動魔法的集中力不會輕易渙散。
更不用說，如同呼吸一般，和理所應當的『生態』一樣的『固有魔法』更不用說。

通過魔法術式的直接介入，如果沒有老師和弟子那樣明確的實力差距是不可能辦到的。但是，如果是干涉只要特定種族才能使用的固有魔法，只有同樣的種族才能辦到。因為只有同樣持有固有技能的同種族才能理解固有魔法。

也就是說，作為人類的菲奧娜對妖精莉莉所使用的固有魔法，是不可能進行直接干涉將其無效化的。

但是，這位魔女將不可能變成了可能。
將那份秘密，毫不隱藏堂堂正正的展現出來，毫無疑問就是被關進水晶中的妖精——莉莉瞬間理解了。但是，這已經是既定事實。

作為菲奧娜對莉莉用的最終兵器打造而成的『妖精殺手』正如其名，有著將妖精唯一且絕對的力量，妖精族的固有魔法無效化的力量。
妖精結界不僅萬能而且十分強大，又因是固有魔法，對莉莉來說是理所當然的。就是處於集中力被打斷劇痛之中，還是在魔力耗盡之前，其堅固的光之結界都會繼續閃耀。

如果『煉獄結界』中沒法做出了結，那就先將莉莉的魔力消耗到所剩無幾再進行戰鬥。如果變成這種場合，僅僅身為人類的菲奧娜和僅僅身為妖精的莉莉，雙方能使用的魔法將會有巨大的差距產生。然後，最終決定勝敗。

對疲憊到連下級魔法都無法使用的菲奧娜來說，直到最後都在一直保護莉莉的妖精結界，無論如何都是阻礙。只有有結界存在，自己就無法戰勝莉莉——在開始摸索殺死莉莉的方法時，一直尋找的，最初且最大的問題。

反過來說，只要沒有破解結界的方法，莉莉就贏定了。
所以，菲奧娜要破除妖精結界。找到消除結界的手段，然後，將它製造出來。

秘密就藏在被關在水晶裡的真正的妖精上。只要是妖精，不論是薇薇安還是別的妖精都可以。
只是作為人類的菲奧娜干涉妖精的固有魔法的必要手段，換而言之中繼器罷了。通過解析同樣擁有固有魔法的妖精，菲奧娜能靠自己的意思和對手的魔法式相連。

當然，如果不是老道的魔法師，是不會這麼簡單就能通過自己的魔法術式進行介入的，就算是直接進行身體接觸，或者通過精神感應等手段進行干涉，也不能簡單的篡改術式。對這方面的防御，也包含在魔法學習的意義之中。這是連魔法學院的一年級學生都知道的常識。

但是，基本無法進行干涉的固有魔法那就另當別論了。就算是身為魔法天才的莉莉，也不會料到居然有人能對自己的固有魔法進行干涉。妖精的固有魔法，是妖精女王伊利斯所賜予的神之魔法。

但是，將活著的妖精作為武器運用，宛如惡魔的智慧，破解了神的魔法。超越莉莉的預測，化為致命一擊。
「啊a——————」
隨著妖精結界的消失，莉莉失去了飛行能力。被正上方刺下的水晶槍刺中，向反方向墜落。
使不上力氣。就這樣被妖精殺手的槍刃刺傷，作為妖精的莉莉已經無法使用任何能力。就連一發光矢，也用不了。

如果變成這樣的話，莉莉就和她看上去一樣，只是一位柔弱的少女。
但是，現在的莉莉，並不是全由美麗的妖精肉體所構成，她的左眼，寄宿著被稱為狂戰士·黑之噩夢的一部分。

『黑之眼珠』，閃閃發光如同燃燒的鬥志。
「――魔弾・榴弾っ！」。
雖然不能使用固有魔法。但是，卻可以使用黑魔法。

僅憑從左眼供給純粹的黑色魔力。莉莉就能如黑乃一樣使用黑魔法。
莉莉左手緊握的『滅星』的槍口，對準了水晶之刃。放出灼熱的黑色榴彈，零距離的爆炸，將脆弱的水晶之刃，變成碎裂的粉塵。
「唔，咕，唔.....」
被打落到地面之前，進行了兩連射。第一發將『妖精殺手』破壞，第二發則射向了菲奧娜。因為是極近距離的射擊，恐怖應該命中了。開槍的自己同樣也處在爆炸範圍內，莉莉幼小的身軀被爆風扇飛，在空中不斷翻滾。

墜落地面後，莉莉一邊發出了痛苦的呻吟，一邊搖搖晃晃的站起來了——
「哈a————！」
此時，從榴彈產生黑煙中，菲奧娜衝了出來。手上的妖精殺手早已粉碎。在零距離爆炸的影響下，戴在頭上的標誌性的三角帽也不見了。

リリィは知っている。あの帽子は魔女の裝備であると共に、空間魔法のかかった保管庫であると。『妖精殺し（リリィ・スレイヤー）』もそこから取り出したに違いない。

雖然，菲奧娜只剩下破破爛爛且被燒焦的魔女的黑衣，早已，沒有新的武器可以取出來。
但是，腹部正中央被開了一個洞的莉莉已經是瀕死的重傷。就像再說要殺死她，僅憑肉體足矣一般——菲奧娜用猛烈的踢擊對向莉莉的腹部，
「咕呼！」
摧殘傷口的一擊。痛的要死，不如說比死了還痛苦。菲奧娜好似描繪了女性曲線美的長腿，寄宿著「疾驅」的效果。通過被強化的腳力使出的踢擊，有著和鉄槌同等的威力衝向莉莉的腹部。
鮮血從莉莉空中噴出，輕易的被踢飛五米
「嘎，哈.....咕，唔，唔....」
早已沒法站起。掙扎的想要站起來的莉莉又倒下，從喉嚨深處涌出的鮮血被大量吐出，劇烈的咳嗽著。
來自腹部的血，和自己吐出的鮮血，化成了倒下的莉莉身邊的血海。
「呼...呼...真是淒慘呢，莉莉」
對菲奧娜來說，就連站在也是一種負擔。一邊氣喘吁吁，一邊邁著沉重的腳步向倒在血海中痛苦掙扎著的莉莉。
雖然身體的疲勞已經臨近極限，但是對除此之外並沒有什麼負傷的菲奧娜來說，將莉莉送入死亡的深淵這點力量還是有的。
「我一直羨慕著你，幼小的姿態惹人怜愛，少女的姿態如此美麗......而且，表面開朗，內心狡猾，一直支持著黑乃」
如今將死的莉莉，能聽到菲奧娜的話語嗎。一邊自言自語，菲奧娜抓起莉莉的頭髮。
「你才是和黑乃最般配的女性。黑乃心中最重要的你。我很羨慕，很嫉妒，我啊，正因為遠不及你——」
將莉莉白金色的頭髮一手抓起，強硬的扯起來。在失去生氣，虛弱的翡翠色和漆黑的瞳孔前，菲奧娜右手握拳。
「暗拳ー」
菲奧娜拳頭上帶著些許黑色的氣場，打向了莉莉的正臉。所剩無幾的魔力，不習慣的黑魔法，更不習慣的拳打腳踢。雖是完全不及黑乃的，幼稚的，徒有其名的直拳，但是要粉碎莉莉的美貌已經綽綽有餘。
「臉色真差呢，那個莉莉，居然會這麼醜陋，變成這副淒慘的樣子.....」
又是一擊。對著臉上噴出鼻血的莉莉，又是右拳打出的一記直拳。
「......我已經，不再羨慕你了」
一拳。
「不再嫉妒你了」
一拳。一拳。
「和黑乃最般配的，是我」
拼盡全力的一拳。
「黑乃最重要的人是我」
灌入愛的一拳。
「黑乃他——」
又一拳，但是，好硬。不，好熱。

莉莉的臉上，稀薄的白色光芒開始閃耀。如同暴風一般激烈毆打下也沒有扭曲的原因，就是這個。

妖精結界的再構築，已經開始了。
「——是我的！」
代替拳頭的毆打，菲奧娜掐住了莉莉纖細的脖子。
「呼...呼,呼呼....」
莉莉的笑聲令人毛骨悚然。
「菲奧娜......你和黑乃......才不般配」
掐住脖子的雙手，感覺到了抵抗感。

普通來說，妖精是全身赤裸的，被白色光膜包裹。當然，這也是妖精結界的效果，好像是最為基礎的部分。對早已穿著衣服的莉莉來說，是無用的機能，平常都是解除著的。

但......原來如此，妖精結界從一開始再構築，這部分開始再生也很自然。

但是，那個堅硬的結界還並未復活。白光，仿彿燃盡最後的生命，隨風即逝一般的弱小。
加大力氣，就能壓制住光的力量，觸碰到莉莉的肌膚。
「去死吧。我要殺掉你，和黑乃永遠的結合在一起」
手上更加用力地掐住。
「咕，唔...呼，唔呼呼，那是不可能的......菲奧娜和黑乃根本不般配......與其和你結合，妮露還更好......」
「沒想到最後的遺言是這種胡說八道的話，真不像你啊，莉莉」
終於，莉莉的光消失了。菲奧娜使出的力量還是照常，不斷掐住快要斷掉的纖細脖子。
「咕...胡說八道嗎...不，這是事實哦」
告訴你吧。沒錯，臉上青白的莉莉告知到。
「——菲奧娜已經，變成無法生育小孩的體質了哦」
「誒」
莉莉訴說著真實的同時，尖銳的槍聲響起。
菲奧娜的雙手，失去了力量。並不是了解莉莉的話語。手腕上飛濺著血沫。
「咕！?」
又是一發。好像從哪裡聽到過，無情的開火聲。

菲奧娜的左手，被高速飛來的鉛製子彈打中。
左右兩邊的手腕，都被槍擊中。
手上傳來的僅僅只是劇痛，並沒有掐死不得不打倒的情敵的觸感。失去力量的雙手從莉莉的脖子上滑落。
「幹得好，艾因，沙瓦伊」
「咕，啊，啊啊....」
一邊痛苦的呻吟，一步，兩步，菲奧娜慢慢後退。
「抓住她，鐸萊，費昂」
「yes，my princess」
完全一樣的男性聲音，完全同時傳到耳中時，菲奧娜早已被困住雙手，抓住了。
周圍沒有任何人。本應如此，兩位身披黑色大衣帶著鐵假面的男人，宛如夾擊一般出現了在菲奧娜兩側。

並不是什麼大不了的事情，像鐵血戰士一樣裝備了消去身姿的魔法具大衣罷了。平常的話，僅僅是看不見的程度，如果靠近的話就會察覺到魔力的氣息。
但是，為了將莉莉逼近走投無路的姿態，將最後的力量都用盡，如此極限狀態下的菲奧娜對周圍的注意也會變得遲鈍。隱去身姿，消去腳步，接近。

然後，菲奧娜被被命名為——『鐸萊』『費昂』的兩位大漢，莉莉創造的『活屍』拘束。
「啊啊，疼的要死啊......フェンフ，快給我『妖精的秘藥』」
「yes，my princess」
又出現一人，和扭曲空間隱蔽系光魔法所造成的獨特現象一起，空無一人的地方出現了帶著鐵面的大漢。從手中的袋子，聽話的在莉莉的腹部灑下了閃光粉末。
「還有，要是被她吟唱魔法就傷腦筋了。塞克斯，把嘴塞上」
「唔呣!」
從後面伸出的寬大手掌，不由分說的捂住了菲奧娜的嘴巴。
「吉本，阿哈特，納尹維持攻擊態勢。雖然我想應該無法進行抵抗了，要是菲奧娜有什麼動作，可以直接攻擊哦」
形式逆轉。莉莉的臉上還是一片青白，還沒有完全恢復，雖然還沾染著鼻血和吐出的鮮血.....那份表情，一直優雅的微笑回來了。
「那麼，菲奧娜，最後稍稍，和你，聊聊吧」
在完全被壓制住，身體動都不能動的菲奧娜面前，莉莉坦蕩的站著。
「說你生育不了孩子，並不是謊言哦，這是真的哦。你自己不也隱隱約約察覺到了嗎?」
並沒有這回事。沒有任何自覺，也沒有感到任何異變。作為魔女，作為冒險者，菲奧娜時常調整和把握自己的身體狀態，

自己的身體沒有異常，作為女性也很正常。
但是，可以推測。
「沒錯，就是黑魔女安迪米昂的加護」
菲奧娜的嘴巴被堵住說不了話。但是，不假思索地想到的東西，擁有精神感應的莉莉，通過話語明確的表達出來。
「正確來說是通過『惡魔存在之證明』進行魔人化的原因呢」
想要全力的行使黑魔女安迪米昂的加護，就必須變身為超越人類的魔人，這就是『惡魔存在之證明』。為了和第七使徒沙利葉，還有最強的情敵莉莉戰鬥，無論如何都必須的加護。
「魔人的肉體和人類的完全不同」
這種道理，不論是誰都一目了然。只要看到長出角，瞳孔顏色也改變了的菲奧娜的身姿，不知內情的村民肯定都會大叫「惡魔！」吧
「身體也好，骨頭也好，神經也.....並且，為了使用魔神的魔法，大腦和靈魂也進行了變質」
菲奧娜才應該是最了解的人。保持人類的肉體，根本無法使用黑魔女安迪米昂所授予的神之魔法，所以需要魔人化。
「所以，這裡也改變了哦」
莉莉直到最後左手都沒有放下的『滅星』，將槍口指向了菲奧娜的下腹部。
「你的子宮，已經不是人類的子宮了。將男人的精子僅僅作為力量而吸收，惡魔的，不，變成了淫魔的子宮。」
「唔呣！」
「沒有騙你哦，決定性的證據，就是你用淫魔的邪道對黑乃吸精。然後，用這份力量在和我的戰鬥中使用.....沒錯，你不久前已經不是『女人』了哦」
莉莉銳利的目光，刺向動搖著的菲奧娜。像在指責菲奧娜犯下了不可饒恕的罪行。
「惡魔怎麼可能懷上人類的孩子。但是，與巴佛米特或迪亞波羅交配的話，說不定能生下小孩哦」
「唔呣！唔——！！」
「這就是，利用黑乃的愛的懲罰哦。啊啊，多麼骯髒啊——魔彈」
莉莉毫無慈悲，扣下了扳機。
「唔咕！」
菲奧娜的身體顫抖。開槍聲混著含糊不清的聲音。射出的黑色彈丸，擊中了惡魔的子宮。
「骯髒，骯髒，骯髒」
射擊，射擊，連射。不停的扣動扳機，射出魔彈。
被強行抑制住的菲奧娜，僅僅只能發出組成不了話語的痛苦呻吟聲，眼淚從金色的雙眸中落下。
「和黑乃永遠結合？比笑死人了。連孩子都生不了的人，只是女人中的廢物」
不，哭泣的原因，肯定不是痛楚。
「明明是比貧民窟的娼婦還要下賤的身體，還想貪圖黑乃的愛什麼的——」
感到痛的，是心。心碎了。菲奧娜作為女性的尊嚴，被打了個粉碎。
「——噁心到胃酸都要吐出來了！！」
在滴血，噗咚噗咚，從菲奧娜白皙的雙腿傳來，鮮血不斷留下。
她身上纏著魔女的斗篷，菲奧娜注入心血製作出的精品。魔法防御不用說，物理防御也非常優秀。正是因為明白這點，莉莉才開槍的吧。
僅憑魔彈一發，是無法貫穿菲奧娜的斗篷。但是，魔彈的所帶來的衝擊還是會傳到身體上。
射出的子彈和莉莉迸發的憤怒，一次又一次打向了菲奧娜的腹部.....
「但是，安心吧，菲奧娜。因為黑乃的孩子由我來生育」
「唔....」
染血的雙腿一邊嘎吱嘎吱的抖動著，菲奧娜發出了聲音。但是，因為嘴巴被頑固地手捂住，無論是否定還是拒絕，都決定形成不了言語。
「我會讓黑乃幸福的」
帶安撫了憤怒，平和的表情，將對向菲奧娜腹部的槍口，轉向了菲奧娜的頭。
「所以啊，你就一直守望著我和黑乃吧。因為，你是我獨一無二的親友哦——」
莉莉扣下扳機的最後一瞬間，菲奧娜好像接受自己全部的命運一般，靜靜的閉上了雙眼。
「——再見，菲奧娜」


=====================================

　２０１６年１２月３０日
　今年最後の更新で、なんとかリリィとフィオナの戦いを決著させることができました。次回で第３０章は最終回となります。
　それでは皆さん、よいお年を！